@extends('public.layouts.main')

@section('meta')
    <title>Регистрация в интернет-магазине Autoslon</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('content')
    <section class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-3 col-md-8 col-md-push-2 col-sm-10 col-sm-push-1">
                    <h3 class="inner-title">Регистрация</h3>
                    @if(!empty($errors->all()))
                        <div class="error-message">
                            {!! $errors->first() !!}
                        </div>
                    @endif
                    <form class="registration-form" method="post">
                        {!! csrf_field() !!}
                        <div class="clearfix">
                            <label for="registration-form__name" class="registration-form__label">Ваше имя:*</label>
                            <input type="text"
                                   name="first_name"
                                   id="registration-form__name"
                                   class="registration-form__input @if($errors->has('first_name')) input-error @endif"
                                   value="{!! old('first_name') !!}"
                            />
                        </div>
                        <div class="clearfix">
                            <label for="registration-form__surname" class="registration-form__label">Ваша фамилия:</label>
                            <input type="text"
                                   id="registration-form__surname"
                                   name="last_name"
                                   class="registration-form__input @if($errors->has('last_name')) input-error @endif"
                                   value="{!! old('last_name') !!}"
                            />
                        </div>
                        <div class="clearfix">
                            <label for="registration-form__phone" class="registration-form__label">Телефон:*</label>
                            <input type="text"
                                   id="registration-form__phone"
                                   name="phone"
                                   class="registration-form__input @if($errors->has('phone')) input-error @endif"
                                   value="{!! old('phone') !!}"
                            />
                        </div>
                        <div class="clearfix">
                            <label for="registration-form__email" class="registration-form__label">E-mail:*</label>
                            <input type="text"
                                   id="registration-form__email"
                                   name="email"
                                   class="registration-form__input @if($errors->has('email')) input-error @endif"
                                   value="{!! old('email') !!}"
                            />
                        </div>
                        <div class="clearfix">
                            <label for="registration-form__password" class="registration-form__label">Придумайте пароль:*</label>
                            <input type="password"
                                   id="registration-form__password"
                                   name="password"
                                   class="registration-form__input @if($errors->has('password')) input-error @endif"
                            />
                        </div>
                        <div class="clearfix">
                            <label for="registration-form__confirm" class="registration-form__label">Подтвердите пароль:*</label>
                            <input type="password"
                                   id="registration-form__confirm"
                                   name="password_confirmation"
                                   class="registration-form__input @if($errors->has('password_confirmation')) input-error @endif"
                            />
                        </div>
                        <button type="submit" class="registration-form__btn">Зарегистрироваться</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection