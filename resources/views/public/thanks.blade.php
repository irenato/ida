@extends('public.layouts.main')
@section('meta')
    <title>Спасибо за заказ</title>
@endsection
@section('content')
    <section class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    @if($confirmed)
                        <h3 class="inner-title">Заказ оформлен!</h3>
                    @else
                        <h3 class="inner-title">Спасибо за заказ!</h3>
                    @endif
                        <div class="thx-text">
                        @if($confirmed)
                            <span class="thx-text__item">Ваш заказ №{!! $order_id !!} уже оформлен.</span>
                        @else
                            <span class="thx-text__item">Ваш заказ №{!! $order_id !!} принят.</span>
                            <span class="thx-text__item">В ближайшее время наши менеджеры свяжутся с вами для уточнения всех деталей.</span>
                        @endif
                        <a href="/" class="continue-btn">Продолжить покупки</a>
                        @if($user)
                            <a href="/user" class="continue-btn">Перейти в личный кабинет</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection