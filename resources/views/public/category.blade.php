@extends('public.layouts.main')
@section('meta')
    <title>{!! $category->meta_title or $category['meta_title'] !!}</title>
    <meta name="description" content="{!! $category->meta_description or '' !!}">
    <meta name="keywords" content="{!! $category->meta_keywords or '' !!}">
    @if($category->meta_robots != '')
        <meta name="robots" content="{{ $category->meta_robots }}" />
    @endif
    @if($category->meta_canonical != '')
        <link rel="canonical" href="{{ $category->meta_canonical }}"/>
    @endif
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('categories', $category) !!}
@endsection

@section('content')
    <div class="container">
        <h2 class="catalog-title">{!! $category->name !!}</h2>
    </div>

    <section class="catalog-content">
        <div class="container">
            <div class="row">
                @if(!empty($filters) || !$products->isEmpty())
                <div class="col-sm-3">
                    <form action="/categories/{!! $category->url_alias !!}" method="get" class="product-filter-form">
                    <aside class="catalog-filters">
                        @if(!is_null($prices))
                            <div class="clearfix catalog-filter__price-wrap">
                                <label for="amount">Цена:</label>
                                <input type="hidden" name="prices[min]" id="min-price" value="{!! $filters['price']['min'] or $prices['min'] !!}">
                                <input type="hidden" name="prices[max]" id="max-price" value="{!! $filters['price']['max'] or $prices['max'] !!}">
                                <input type="text" id="amount" readonly class="catalog-filter__price" >
                            </div>
                            <div id="slider-range" data-min="{!! $prices['min'] !!}" data-max="{!! $prices['max'] !!}"></div>
                        @endif
                        <div class="catalog-content__form-model">
                            <span class="catalog-content__form-title">Модель Вашего авто</span>
                            <select name="">
                                <option selected>Марка авто</option>
                                <option>фвывап</option>
                                <option>фывап</option>
                            </select>
                            <select name="">
                                <option selected>Модель авто</option>
                                <option>фвывап</option>
                                <option>фывап</option>
                            </select>
                            <select name="">
                                <option selected>Год выпуска</option>
                                <option>фвывап</option>
                                <option>фывап</option>
                            </select>
                            <button type="submit" class="catalog-content__btn">Подобрать</button>
                        </div>
                        @if(!empty($attributes))
                            @foreach($attributes as $i => $attribute)
                                <div class="filter-check-list-wrap">
                                    <span class="filter-check-title"><i class="filter-check-title__icon">&#xe80b</i>{!! $attribute->name !!}</span>
                                    <ul class="filter-check-list">
                                        @foreach($attribute->values as $j => $filter)
                                            <li class="filter-check-item">
                                                <input type="checkbox"
                                                       name="filters[]"
                                                       value="{!! $attribute->id !!}_{!! $filter->id !!}"
                                                       class="filter-check-item__input filter-input" id="filter-check-item-{!! $i !!}__input-{!! $j !!}"
                                                       @if(isset($filters['attributes'][$attribute->id]) && in_array($filter->id, $filters['attributes'][$attribute->id])) checked @endif
                                                />
                                                <label for="filter-check-item-{!! $i !!}__input-{!! $j !!}" class="filter-check-item__label">{!! $filter->name !!}</label>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </aside>
                    </form>
                </div>
                @endif
                @if(!$products->isEmpty())
                <div class="col-sm-9">
                    <div class="row">
                        <form class="sorting-form" method="get">
                            <label for="sorting-form__select" class="sorting-form__label">Сортировать по</label>
                            <select name="sorting" onchange="sortBy($(this).val())">
                                @foreach($sort_array as $sort)
                                    <option value="{!! $sort['value'] !!}" @if($sort['value'] == $current_sort) selected @endif>{!! $sort['name'] !!}</option>
                                @endforeach
                            </select>
                        </form>
                        <div class="catalog-cards clearfix" id="category-content">
                            @foreach($products as $product)
                                @include('public.layouts.product', ['product' => $product])
                            @endforeach
                        </div>
                    </div>

                        {!! $products->links() !!}

                </div>
                @else
                    <div class="@if(!empty($filters) || !$products->isEmpty()) col-sm-9 @else col-sm-12 @endif">
                        <div class="error-message">
                            <div class="error-message error-message__text">
                                В этой категории пока нет товаров!
                            </div>
                        </div>
                    </div>
                @endif

                <div class="@if(!empty($filters) || !$products->isEmpty()) col-sm-9 @else col-sm-12 @endif">
                    <div class=" error-message__text">
                        {{ $category->content }}
                    </div>
                </div>

            </div>
        </div>

        @if($viewed !== null)
        <section class="recommendation-products">
            <div class="container">
                <div class="title-wrap">
                    <h2 class="section-title">Просмотренные товары</h2>
                </div>
                <div class="product-slider">
                    @foreach($viewed as $product)
                        @include('public.layouts.product', ['product' => $product, 'slider' => true])
                    @endforeach
                </div>
            </div>
        </section>
        @endif
    </section>
@endsection