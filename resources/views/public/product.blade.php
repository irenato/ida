@extends('public.layouts.main')
@section('meta')
    <title>{!! $product->meta_title !!}</title>
    <meta name="description" content="{!! $product->meta_description !!}">
    <meta name="keywords" content="{!! $product->meta_keywords !!}">
@endsection

{{--@section('breadcrumbs')--}}
    {{--{!! Breadcrumbs::render('product', $product, $product->category) !!}--}}
{{--@endsection--}}

@section('content')

    <section class="product-content">
        <div class="container">
            <div class="col-sm-5">
                <div class="product-thumb">
                    <div class="product-thumb__slider">
                        @foreach($gallery as $image)
                        <div class="product-thumb__slider-item">
                            <a href="{!! $image->get_current_file_url('full') !!}" class="product-thumb__link"  style="background-image: url({!! $image->get_current_file_url('product') !!});"></a>
                        </div>
                        @endforeach
                    </div>
                    <div class="product-thumb__slider-nav">
                        @foreach($gallery as $image)
                        <div class="product-thumb__slider-nav-item">
                            <span class="product-thumb__nav-link"  style="background-image: url({!! $image->get_current_file_url('product') !!});"></span>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="product-short-info">
                    <div class="product-short-info__block">
                        <h4 class="product-short-info__name">{!! $product->name !!}</h4>
                        <ul class="product-cart__star-list product-cart__star-list_left clearfix">
                            @for($i=1; $i<=5; $i++)
                                {{--@break($product->rating == null)--}}
                                @if($i <= $product->rating)
                                    <li class="product-cart__star"><i class="close-popup-btn__star-icon">&#xe809;</i></li>
                                @else
                                    <li class="product-cart__star"><i class="close-popup-btn__star-icon">&#xe80a;</i></li>
                                @endif
                            @endfor
                        </ul>
                    </div>
                    <div class="product-short-info__block">
                        <ul class="product-short-info__attributes">
                            <li class="product-short-info__attributes-item">Автомобиль: Ford Focus 2004-2008</li>
                            <li class="product-short-info__attributes-item">Производитель: выаыва</li>
                            <li class="product-short-info__attributes-item">Цвет: марсала</li>
                        </ul>
                        <span class="product-short-info__text">{!! $product->short_description or '' !!}</span>
                    </div>
                    <form class="product-short-info__bottom">
                        <div class="col-md-2 col-xs-4">
                            <div class="product-short-info__amount">
                                <span class="product-short-info__amount-inc product-short-info__amount-inc_minus"><i class="product-short-info__amount-icon">&#xe80e</i></span>
                                <input type="text" class="product-short-info__amount-input" id="product-card-quantity" value="1" readonly>
                                <span class="product-short-info__amount-inc product-short-info__amount-inc_plus"><i class="product-short-info__amount-icon">&#xe80d</i></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-8">
                            <span class="product-short-info__price">{!! $product->price !!} грн</span>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="clearfix">
                                <a href="javascript:void(0)" class="product-cart__btn btn_buy" data-product-id="{!! $product->id !!}">Купить</a>
                                @if(empty($account_wishlist))
                                    <a href="javascript:void(0)"
                                        class="product-cart__fav button-wishlist"
                                        data-product-id="{!! $product->id !!}"
                                        @if($user && in_array($product->id, $user->wishlist('array')))
                                            data-action="remove"
                                        @else
                                            data-action="add"
                                        @endif
                                    >
                                        <i class="close-popup-btn__fav-icon">
                                            @if($user && in_array($product->id, $user->wishlist('array')))
                                                &#xe801;
                                            @else
                                                &#xe800;
                                            @endif
                                        </i>
                                    </a>
                                    <div class="cart-hover fav-hover wishlist-error">
                                        <span class="cart-hover__text">Войдите, чтобы сохранять понравившиеся Вам товары</span>
                                        <a href="/login" class="cart-hover__btn">Войти</a>
                                        <a href="/register" class="cart-hover__cart-link">Зарегистрироваться</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="product-tabs">
            <div class="container">
                <ul class="product-tabs-caption">
                    <li class="product-tabs-caption__item active">Описание</li>
                    <li class="product-tabs-caption__item">Характеристики</li>
                    <li class="product-tabs-caption__item">Отзывы</li>
                </ul>
                <article class="product-tabs-content active">
                    {!! $product->description or '' !!}
                </article>
                <article class="product-tabs-content">
                    <ul class="product-characteristics">
                        @forelse($product->attributes as $attribute)
                            <li class="product-characteristics__item">
                                <span class="product-characteristics__item-left">{!! $attribute->info->name !!}:</span>
                                <span class="product-characteristics__item-right">{!! $attribute->value->name !!}</span>
                            </li>
                        @empty
                            <li class="product-characteristics__item">
                                <span class="product-characteristics__item-left">У данного товара характеристики отсутствуют!</span>
                            </li>
                        @endforelse
                    </ul>
                </article>
                <article class="product-tabs-content">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="javascript:void(0)" class="review-btn"
                                @if(!$user)
                                    onmouseenter="$('.cart-hover').removeClass('active'); $(this).next('.cart-hover').addClass('active');"
                                @else
                                    onclick="$(this).parent().find('.review-form').slideDown('slow');"
                                @endif
                            >Оставить отзыв</a>
                            <div class="cart-hover usefull-hover" style="top: 55px;" onmouseleave="$(this).removeClass('active')">
                                <span class="cart-hover__text">Для того, чтобы оставить отзыв, необходимо</span>
                                <a href="/login" class="cart-hover__btn">Войти</a>
                                <span class="cart-hover__text">или</span>
                                <a href="/register" class="cart-hover__cart-link" style="display: block;">Зарегистрироваться</a>
                            </div>
                            <form class="review-form">
                                {!! csrf_field() !!}
                                <input type="hidden" name="type" value="review">
                                <input type="hidden" name="product_id" value="{!! $product->id !!}">
                                <h3 class="review-form__title">Оставить отзыв</h3>
                                <div class="error-message" id="error-review" style="display: none;">
                                    <div class="error-message__text"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="review-form__input_name" class="review-form__label">Ваше имя</label>
                                        <input type="text" id="review-form__input_name" class="review-form__input" name="name" value="{!! $user->first_name or '' !!}">
                                        <label for="review-form__input_email" class="review-form__label">Ваш Email</label>
                                        <input type="text" id="review-form__input_email" class="review-form__input" name="email" value="{!! $user->email or '' !!}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="review-form__input_comment" class="review-form__label">Комментарий</label>
                                        <textarea rows="4" id="review-form__input_comment" class="review-form__input_comment review-form__input" name="review"></textarea>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="give-review__rating">
                                            <span class="give-review__text">Оценить:</span>
                                            <input id="star-5" type="radio" name="grade" class="give-review__rating-input" value="5">
                                            <label for="star-5" class="give-review__rating-label"></label>

                                            <input id="star-4" type="radio" name="grade" class="give-review__rating-input" value="4">
                                            <label for="star-4" class="give-review__rating-label"></label>

                                            <input id="star-3" type="radio" name="grade" class="give-review__rating-input" value="3">
                                            <label for="star-3" class="give-review__rating-label"></label>

                                            <input id="star-2" type="radio" name="grade" class="give-review__rating-input" value="2">
                                            <label for="star-2" class="give-review__rating-label"></label>

                                            <input id="star-1" type="radio" name="grade" class="give-review__rating-input" value="1">
                                            <label for="star-1" class="give-review__rating-label"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-push-6">
                                        <div class="clearfix">
                                            <button type="submit" class="review-form__btn">Оставить отзыв</button>
                                            <a href="javascript:void(0)" class="review-form__btn review-form__btn_cancel">Отмена</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <ul class="review-list">
                                @forelse($reviews as $review)
                                <li class="review-item">
                                    <div class="review-item__top clearfix">
                                        <div class="review-item__top-left">
                                            <span class="review-item__name">{!! $review['parent']->user->first_name !!}</span>
                                            <ul class="product-cart__star-list">
                                                @for($i=1; $i<=5; $i++)
                                                    @if($i <= $review['parent']->grade)
                                                        <li class="product-cart__star"><i class="close-popup-btn__star-icon">&#xe809;</i></li>
                                                    @else
                                                        <li class="product-cart__star"><i class="close-popup-btn__star-icon">&#xe80a;</i></li>
                                                    @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="review-item__date">{!! $review['parent']->date !!}</div>
                                    </div>
                                    <div class="review-item__text">
                                        <p>{!! $review['parent']->review !!}</p>
                                    </div>
                                    <div class="review-item__bottom clearfix">
                                        <span class="review-item__reply-btn-wrap">
                                            <div class="cart-hover usefull-hover" onmouseleave="$(this).removeClass('active')">
                                                <span class="cart-hover__text">Для того, чтобы оставить комментарий, необходимо</span>
                                                <a href="/login" class="cart-hover__btn">Войти</a>
                                                <span class="cart-hover__text">или</span>
                                                <a href="/register" class="cart-hover__cart-link">Зарегистрироваться</a>
                                            </div>
                                            <a href="javascript:void(0)" class="review-item__reply-btn"
                                                @if(!$user)
                                                    onmouseenter="$('.cart-hover').removeClass('active'); $(this).prev('.cart-hover').addClass('active');"
                                                @else
                                                    onclick="$(this).parent().parent().find('.answer-form').slideDown('slow');"
                                                @endif
                                            >Ответить</a>
                                        </span>
                                        <ul class="review-item__useful">
                                            <span class="review-item__useful-text">Отзыв полезен?</span>
                                            <li class="review-item__useful-item review-item__useful-item_yes">
                                                <a href="javascript:void(0)"
                                                   id="add-like" @if(!$user) class="unregistered" @endif
                                                   data-review="{!! $review['parent']->id !!}"
                                                   data-action="like">Да({!! count(json_decode($review['parent']->like)) !!})</a>
                                            </li> /
                                            <li class="review-item__useful-item review-item__useful-item_no">
                                                <a href="javascript:void(0)"
                                                   id="add-dislike" @if(!$user) class="unregistered" @endif
                                                   data-review="{!! $review['parent']->id !!}"
                                                   data-action="dislike">Нет({!! count(json_decode($review['parent']->dislike)) !!})</a>
                                            </li>
                                            <div class="cart-hover usefull-hover" onmouseleave="$(this).removeClass('active')">
                                                <span class="cart-hover__text">Для того, чтобы поставить оценку, необходимо</span>
                                                <a href="/login" class="cart-hover__btn">Войти</a>
                                                <span class="cart-hover__text">или</span>
                                                <a href="/register" class="cart-hover__cart-link">Зарегистрироваться</a>
                                            </div>
                                        </ul>
                                        <form class="answer-form">
                                            <h3 class="review-form__title">Оставить ответ</h3>
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="type" value="answer">
                                            <input type="hidden" name="product_id" value="{!! $product->id !!}">
                                            <input type="hidden" name="parent_review_id" value="{!! $review['parent']->id !!}">
                                            <div class="error-message" id="error-answer" style="display: none;">
                                                <div class="error-message__text"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="review-form__input_name" class="review-form__label">Ваше имя</label>
                                                    <input type="text" id="review-form__input_name" name="name" class="review-form__input" value="{!! $user->first_name or '' !!}">
                                                    <label for="review-form__input_email" class="review-form__label">Ваш Email</label>
                                                    <input type="text" id="review-form__input_email" class="review-form__input" name="email" value="{!! $user->email or '' !!}">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="review-form__input_comment" class="review-form__label">Комментарий</label>
                                                    <textarea rows="4" id="review-form__input_comment" class="review-form__input_comment review-form__input" name="review"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-push-6">
                                                    <div class="clearfix">
                                                        <button type="submit" class="review-form__btn">Ответить</button>
                                                        <a href="javascript:void(0)" class="review-form__btn review-form__btn_cancel answer-form__btn_cancel">Отмена</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    @if(!empty($review['comments']))
                                        @foreach($review['comments'] as $comment)
                                        <div class="review-item__answer">
                                            <div class="review-item__top clearfix">
                                                <div class="review-item__top-left">
                                                    <span class="review-item__name">{!! $comment->user->first_name !!}</span>
                                                </div>
                                                <div class="review-item__date">{!! $comment->date !!}</div>
                                            </div>
                                            <div class="review-item__text">
                                                <p>{!! $comment->review !!}</p>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                </li>
                                @empty
                                <li class="review-item">
                                    <div class="review-item__top clearfix">
                                        <div class="review-item__top-left">
                                            <span class="review-item__name">У этого товара пока нет отзывов! Будьте первым!</span>
                                        </div>
                                    </div>
                                </li>
                                @endforelse
                            </ul>
                            <a href="javascript:void(0)" class="all-review-btn">Смотреть все отзывы</a>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        @if($viewed !== null)
            <section class="recommendation-products">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="section-title">Просмотренные товары</h2>
                    </div>
                    <div class="product-slider">
                        @foreach($viewed as $product)
                            @include('public.layouts.product', ['product' => $product, 'slider' => true])
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
    </section>


@endsection