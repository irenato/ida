@extends('public.layouts.main')
@section('meta')
    <title>{!! $article->meta_title !!}</title>
    <meta name="description" content="{!! $article->meta_description !!}">
    <meta name="keywords" content="{!! $article->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('articles_item', $article) !!}
@endsection

@section('content')
    <section class="articles">
        <div class="container">
            <div class="title-wrap">
                <h2 class="section-title">{!! $article->title !!}</h2>
            </div>
            <div class="col-md-8 col-md-push-2 col-sm-10 col-sm-push-1">
                <div class="articles-item">
                    <a href="javascript:void(0)" class="articles-item__thumb articles-item__thumb_high" style="background-image: url('{!! $article->image->get_current_file_url('full') !!}');"></a>
                    <span class="articles-item__date">{!! $article->date !!}</span>
                    <span class="articles-item__descr">{!! $article->subtitle or '' !!}</span>
                    <span class="articles-item__text">{!! html_entity_decode($article->text) !!}</span>
                    <div class="articles-item__btn-wrap">
                        @if($previous)
                            <a href="/news/{!! $previous !!}" class="articles-item__step-btn"><i class="close-popup-btn__step-icon">&#xe80c</i></a>
                        @endif
                        <a href="/news" class="articles-item__btn">К списку новостей</a>
                        @if($next)
                            <a href="/news/{!! $next !!}" class="articles-item__step-btn"><i class="close-popup-btn__step-icon">&#xe80b</i></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection