@extends('public.layouts.main')
@section('meta')
    <title>Оформление заказа</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection
@section('content')
    <section class="checkout-content">
        <div class="container">
            @if(!is_null($cart))
            <h3 class="inner-title inner-title_upper">Оформление заказа</h3>
            <div class="row">
                <div class="col-md-6 col-md-push-6">
                    <ul class="checkout-order">
                        <li class="checkout-order__item clearfix">
                            <span class="checkout-order__title">Ваш заказ</span>
                            <!-- <a href="javascript:void(0)" class="checkout-order__link">Редактировать заказ</a> -->
                        </li>
                        <li class="checkout-order__item clearfix">
                            <div id="order_cart_content">
                                @include('public.layouts.cart', ['checkout' => true])
                            </div>
                        </li>
                        <li class="checkout-order__item clearfix">
                            <span class="checkout-order__price-total">Итого: <span id="order_cart-price">{!! $cart->total_price !!}</span> грн</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <div class="tabs">
                        @if(!$user)
                        <ul class="tabs-caption checkout-tabs-caption clearfix">
                            <li class="tabs-caption__item checkout-tabs-caption__item active">Новый покупатель</li>
                            <li class="tabs-caption__item checkout-tabs-caption__item">Я постоянный клиент</li>
                        </ul>
                        @endif
                        <article class="tabs-content checkout-tabs-content active">

                            <div class="checkout-step">
                                <span class="checkout-step__title"><span class="checkout-step__title-numb">1</span>Личная информация</span>
                                <span class="checkout-step__edit hidden">Редактировать данные</span>
                                <div class="checkout-step__body checkout-step__body_first">
                                    <form id="checkout-personal-info">
                                        @if(!$user)
                                        <ul class="checkout-form__registration-switcher">
                                            <li class="checkout-form__switch-item">
                                                <input type="radio" name="checkout_registration" id="with-registration" class="checkout-form__radio" value="true" checked>
                                                <label for="with-registration" class="checkout-form__radio-label">Регистрация</label>
                                            </li>
                                            <li class="checkout-form__switch-item">
                                                <input type="radio" name="checkout_registration" id="without-registration" value="false" class="checkout-form__radio">
                                                <label for="without-registration" class="checkout-form__radio-label">Без регистрации</label>
                                            </li>
                                        </ul>
                                        @endif
                                        <div class="error-message" style="display: none;">
                                            <div class="error-message__text"></div>
                                        </div>
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="checkout-step__name" class="checkout-step__label">Ваше имя:*</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="checkout-step__input" id="checkout-step__name" name="first_name" value="{!! $user->first_name or '' !!}" />
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="checkout-step__lastname" class="checkout-step__label">Ваша фамилия:</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="checkout-step__input" id="checkout-step__lastname" name="last_name" value="{!! $user->last_name or '' !!}" />
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="checkout-step__phone" class="checkout-step__label">Телефон:*</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="checkout-step__input" id="checkout-step__phone" name="phone" value="{!! $user->phone or '' !!}">
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="checkout-step__email" class="checkout-step__label">E-mail:*</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="checkout-step__input" id="checkout-step__email" name="email" value="{!! $user->email or '' !!}">
                                            </div>
                                            @if(!$user)
                                            <div class="checkout-form__password">
                                                <div class="col-sm-6">
                                                    <input type="password" class="checkout-step__input" id="checkout-step__password" name="password" placeholder="Пароль">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="password" class="checkout-step__input" id="checkout-step__password_r" name="password_confirmation" placeholder="Повторите пароль">
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-sm-4 col-sm-push-8">
                                                <input type="hidden" id="current_order_id" name="current_order_id" value="{!! $current_order_id or '0' !!}">
                                                <button type="submit" class="mc_buy_btn checkout-step__submit-btn">Далее</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="checkout-step">
                                <span class="checkout-step__title"><span class="checkout-step__title-numb">2</span>Оплата и доставка</span>
                                <form id="checkout-step-2">
                                    {!! csrf_field() !!}
                                    <input type="hidden" id="current_order_id_2" name="current_order_id" value="{!! $current_order_id or '0' !!}">
                                    <div class="checkout-step__body checkout-step__body_second">
                                        <div class="error-message" style="display: none;">
                                            <div class="error-message__text"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="checkout-step__delivery" class="checkout-step__label">Способ доставки:</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <select id="checkout-step__delivery" class="checkout-step__select checkout-step__payment-select" name="delivery">
                                                    <option value="0">Выберите...</option>
                                                    <option value="newpost">Новая почта</option>
                                                    <option value="ukrpost">Укрпочта</option>
                                                    <option value="courier">Курьером по Харькову</option>
                                                    <option value="pickup">Самовывоз</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="checkout-delivery-payment"></div>
                                    </div>
                                </form>
                            </div>
                        </article>

                        <article class="tabs-content checkout-tabs-content">
                            <form class="checkout-login" action="/login" method="post">
                                <div class="row">
                                    @if(!empty($errors->all()))
                                        <div class="error-message">
                                            <div class="error-message__text">{!! $errors->first() !!}</div>
                                        </div>
                                    @endif
                                    {!! csrf_field() !!}
                                    <div class="col-sm-6">
                                        <label for="checkout-login__input_login" class="checkout-login__label">E-mail</label>
                                        <input type="text" id="checkout-login__input_login" class="checkout-login__input" name="email">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="checkout-login__input_password" class="checkout-login__label">Пароль</label>
                                        <input type="password" id="checkout-login__input_password" class="checkout-login__input" name="password">
                                        <a href="/forgotten" class="checkout-login__forgot">Забыли пароль?</a>
                                    </div>
                                    <div class="col-sm-6 col-sm-push-6">
                                        <input type="hidden" name="action" value="checkout">
                                        <button type="submit" class="checkout-login__btn">Войти</button>
                                    </div>
                                </div>
                            </form>
                        </article>
                    </div>
                </div>
                @else
                    <h3 class="inner-title inner-title_upper">Корзина пуста!</h3>
                    <div class="row">
                    <div class="col-md-6 col-md-push-3">
                        <div class="thx-text">
                            <a href="/" class="continue-btn">Продолжить покупки</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection