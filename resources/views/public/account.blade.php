@extends('public.layouts.main')

@section('meta')
    <title>Личный кабинет</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('content')
    <section class="account-content">
        <div class="tabs">
            <div class="container">
                <h3 class="aside__title">Личный кабинет</h3>
                <div class="row">
                    <div class="col-sm-3">
                        <aside class="account-aside">
                            <ul class="tabs-caption">
                                <li class="tabs-caption__item active">Персональные данные</li>
                                <li class="tabs-caption__item">История заказов</li>
                                <li class="tabs-caption__item" data-action="wishlist">Список закладок</li>
                                <li class="tabs-caption__item">Сменить данные</li>
                                <li><a href="/logout" class="tabs-caption__out-link"><i class="tabs-caption__icon">&#xe802</i>Выход</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-sm-9">
                        <article class="tabs-content active">
                            <h4 class="tabs-content__title">Персональные данные</h4>
                            <div class="row">
                                <div class="personal-data__wrap clearfix">
                                    <div class="col-sm-3">
                                        <span class="personal-data__item  personal-data__item_left">Ваше имя</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <span class="personal-data__item  personal-data__item_right">{!! $user->first_name or '' !!}</span>
                                    </div>
                                </div>
                                <div class="personal-data__wrap clearfix">
                                    <div class="col-sm-3">
                                        <span class="personal-data__item  personal-data__item_left">Ваша фамилия</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <span class="personal-data__item  personal-data__item_right">{!! $user->last_name or '' !!}</span>
                                    </div>
                                </div>
                                <div class="personal-data__wrap clearfix">
                                    <div class="col-sm-3">
                                        <span class="personal-data__item  personal-data__item_left">Телефон</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <span class="personal-data__item  personal-data__item_right">{!! $user->phone or '' !!}</span>
                                    </div>
                                </div>
                                <div class="personal-data__wrap clearfix">
                                    <div class="col-sm-3">
                                        <span class="personal-data__item  personal-data__item_left">E-mail</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <span class="personal-data__item  personal-data__item_right">{!! $user->email or '' !!}</span>
                                    </div>
                                </div>
                                <div class="personal-data__wrap clearfix">
                                    <div class="col-sm-3">
                                        <span class="personal-data__item  personal-data__item_left">Город</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <span class="personal-data__item  personal-data__item_right">{!! $user->user_data->address()->city or '' !!}</span>
                                    </div>
                                </div>
                                <div class="personal-data__wrap clearfix">
                                    <div class="col-sm-3">
                                        <span class="personal-data__item  personal-data__item_left">Адрес</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <span class="personal-data__item  personal-data__item_right">{!! $user->user_data->address()->address or '' !!}</span>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tabs-content">
                            <h4 class="tabs-content__title">История заказов</h4>
                            @forelse($orders as $order)
                                <div class="history-item">
                                    <div class="history-item__header clearfix">
                                        <ul class="history-item__header-left">
                                            <li class="history-item__header-item">Заказ №{!! $order->id !!}</li>
                                            <li class="history-item__header-item">{!! $order->date !!}</li>
                                            <li class="history-item__header-item">{!! $order->time !!}</li>
                                        </ul>
                                        <span class="history-item__header-right history-item__header-right_ready">{!! $order->status->status or 'незавершенный' !!}</span>
                                    </div>
                                    <ul class="history-item__order-list">
                                        @foreach($order->getProducts() as $item)
                                            <li class="history-item__order-info">
                                                <a href="{!! $item['product']->url_alias !!}" class="history-item__order-name">{!! $item['product']->name !!}</a>
                                                <span class="history-item__order-name history-item__order-name_right">{!! $item['product']->price * $item['quantity'] !!} грн</span>
                                                <span class="history-item__order-name history-item__order-name_right">{!! $item['quantity'] !!} шт.</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <ul class="history-item__result-list">
                                        <li class="history-item__result">Количество: {!! $order->total_quantity !!} шт</li>
                                        <li class="history-item__result">Итого: {!! $order->total_price !!} грн</li>
                                        @if($order->status_id == 0)
                                            <li class="history-item__result">
                                                <a href="/checkout" class="personal-data-form__btn" style="margin-top: 10px;">Оформить заказ</a>
                                            </li>
                                        @endif
                                    </ul>

                                </div>
                            @empty

                            @endforelse
                        </article>
                        <article class="tabs-content" data-action="wishlist">
                            <h4 class="tabs-content__title">Список закладок</h4>
                            <div class="row">
                                @foreach($user->wishlist() as $product)
                                    @include('public.layouts.product', [
                                        'product' => $product,
                                        'account_wishlist' => true
                                        ])
                                @endforeach

                                <div class="error-message wishlist-error @if(!empty($user->wishlist('array'))) hidden @endif">
                                    <div class="error-message__text">Список закладок пуст!</div>
                                </div>
                            </div>
                        </article>
                        <article class="tabs-content">
                            <h4 class="tabs-content__title">Сменить данные</h4>
                            <form class="personal-data-form" id="personal-account-info">
                                <div class="error-message hidden">
                                    <div class="error-message__text"></div>
                                </div>
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="personal-data__wrap clearfix">
                                        <div class="col-sm-3">
                                            <span class="personal-data__item personal-data__item_left">Ваше имя:</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   name="first_name"
                                                   class="personal-data-form__input"
                                                   value="{!! $user->first_name or '' !!}"
                                            />
                                        </div>
                                    </div>
                                    <div class="personal-data__wrap clearfix">
                                        <div class="col-sm-3">
                                            <span class="personal-data__item personal-data__item_left">Ваша фамилия:</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   name="last_name"
                                                   class="personal-data-form__input"
                                                   value="{!! $user->last_name or '' !!}"
                                            />
                                        </div>
                                    </div>
                                    <div class="personal-data__wrap clearfix">
                                        <div class="col-sm-3">
                                            <span class="personal-data__item  personal-data__item_left">Телефон</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   name="phone"
                                                   class="personal-data-form__input"
                                                   value="{!! $user->phone or '' !!}"
                                            />
                                        </div>
                                    </div>
                                    <div class="personal-data__wrap clearfix">
                                        <div class="col-sm-3">
                                            <span class="personal-data__item  personal-data__item_left">Email</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   name="email"
                                                   class="personal-data-form__input"
                                                   value="{!! $user->email or '' !!}"
                                            />
                                        </div>
                                    </div>
                                    <div class="personal-data__wrap clearfix">
                                        <div class="col-sm-3">
                                            <span class="personal-data__item  personal-data__item_left">Город</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   name="city"
                                                   class="personal-data-form__input"
                                                   value="{!! $user->user_data->address()->city or '' !!}"
                                            />
                                        </div>
                                    </div>
                                    <div class="personal-data__wrap clearfix">
                                        <div class="col-sm-3">
                                            <span class="personal-data__item  personal-data__item_left">Адрес</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   name="address"
                                                   class="personal-data-form__input"
                                                   value="{!! $user->user_data->address()->address or '' !!}"
                                            />
                                        </div>
                                    </div>
                                    <ul class="checkout-form__registration-switcher checkout-form__registration-switcher_left">
                                        <li class="checkout-form__switch-item">
                                            <input type="radio" name="registration" id="with-registration" class="checkout-form__radio">
                                            <label for="with-registration" class="checkout-form__radio-label">Сменить пароль</label>
                                        </li>
                                        <li class="checkout-form__switch-item">
                                            <input type="radio" name="registration" id="without-registration" class="checkout-form__radio" checked>
                                            <label for="without-registration" class="checkout-form__radio-label">Не менять пароль</label>
                                        </li>
                                    </ul>
                                    <div class="checkout-form__password" style="display: none">
                                        <div class="personal-data__wrap clearfix">
                                            <div class="col-sm-3">
                                                <span class="personal-data__item  personal-data__item_left">Пароль</span>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="password"
                                                       name="password"
                                                       class="personal-data-form__input"
                                                />
                                            </div>
                                        </div>
                                        <div class="personal-data__wrap clearfix">
                                            <div class="col-sm-3">
                                                <span class="personal-data__item  personal-data__item_left">Повторите пароль</span>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="password"
                                                       name="password_confirmation"
                                                       class="personal-data-form__input"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="personal-data-form__btn">Сохранить</button>
                            </form>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection