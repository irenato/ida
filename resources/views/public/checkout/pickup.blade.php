<div class="row">
    <div class="col-sm-4">
        <label for="checkout-step__payment" class="checkout-step__label">Способ оплаты</label>
    </div>
    <div class="col-sm-8">
        <select name="payment" id="checkout-step__payment" class="checkout-step__select">
            <option value="cash">Наличными при самовывозе</option>
            <option value="prepayment">Предоплата</option>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-sm-5 col-sm-push-7">
        <input type="hidden" value="{!! $current_order_id or '0' !!}">
        <button type="submit" class="checkout-step__submit-btn">Оформить заказ</button>
    </div>
</div>