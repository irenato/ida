<div id="newpost-checkout-selection">
    <div class="row">
        <div class="col-sm-4">
            <label for="checkout-step__region" class="checkout-step__label">Выберите область:</label>
        </div>
        <div class="col-sm-8">
            <select id="checkout-step__region" class="checkout-step__select" name="newpost[region]" onchange="newpostUpdate('region', $(this).val());">
                <option value="0">Выберите...</option>
                @foreach($regions as $region)
                    <option value="{!! $region->id !!}">{!! $region->name !!}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <label for="checkout-step__city" class="checkout-step__label">Выберите город:</label>
        </div>
        <div class="col-sm-8">
            <select id="checkout-step__city" class="checkout-step__select" name="newpost[city]" onchange="newpostUpdate('city', $(this).val());">
                <option value="0">Сначала выберите область!</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <label for="checkout-step__warehouse" class="checkout-step__label">Выберите отделение:</label>
        </div>
        <div class="col-sm-8">
            <select id="checkout-step__warehouse" class="checkout-step__select" name="newpost[warehouse]">
                <option value="0">Сначала выберите город!</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <label for="checkout-step__payment" class="checkout-step__label">Способ оплаты</label>
    </div>
    <div class="col-sm-8">
        <select name="payment" id="checkout-step__payment" class="checkout-step__select">
            <option value="cash">Наличными при получении</option>
            <option value="prepayment">Предоплата</option>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-sm-5 col-sm-push-7">
        <input type="hidden" value="{!! $current_order_id or '0' !!}">
        <button type="submit" class="checkout-step__submit-btn">Оформить заказ</button>
    </div>
</div>