@extends('public.layouts.main')
@section('meta')
    <title>{!! $content->meta_title !!}</title>
    <meta name="description" content="{!! $content->meta_description !!}">
    <meta name="keywords" content="{!! $content->meta_keywords !!}">
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="title-wrap">
                <h2 class="section-title">{!! $content->name !!}</h2>
            </div>
        </div>
        <div class="container">
            {!! html_entity_decode($content->content) !!}
        </div>
    </section>
@endsection