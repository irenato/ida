@extends('public.layouts.main')
@section('meta')
    <title>Новости</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('articles') !!}
@endsection

@section('content')
    <section>
        <div class="container">
            @if($articles !== null)
                <div class="title-wrap">
                    <h2 class="section-title">Новости</h2>
                </div>
                <div class="row">
                    @foreach($articles as $article)
                        @include('public.layouts.news', ['article' => $article])
                    @endforeach
                </div>
                {!! $articles->links() !!}
            @else
                <div class="row">
                    <div class="col-sm-12">
                        <div class="error-message">
                            <div class="error-message__text">Еще нет добавленных новостей!</div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection