<section class="header-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-push-3 col-sm-4 col-xs-4">
                <ul class="header-middle__phones clearfix">
                    @forelse($settings->phones as $i => $phone)
                        <li class="header-middle__phones-item">
                            {!! $i === 0 ? '<i class="header-middle__phones-ico">&#xe804</i>' : '' !!}
                            {!! $phone !!}
                        </li>
                    @empty
                    @endforelse
                </ul>
                <form class="header-middle__search" action="search" method="post">
                    {!! csrf_field() !!}
                    <input type="text" name="search" class="header-middle__search-input" placeholder="Найдите товары">
                    <button type="submit" class="header-middle__search-btn"><i class="header-middle__search-ico">&#xe807</i></button>
                    <a href="javascript:void(0)" class="close-popup-btn popup-mobile-btn" data-popup=".header-middle__search">
                        <i class="close-popup-btn__icon close-popup-btn__icon_black">&#xe808</i>
                    </a>
                </form>
                <ul class="header-middle__icons header-middle__icons_left">
                    <li class="header-middle__icons-item">
                        <a href="javascript:void(0)" class="header-middle__icons-link popup-mobile-btn" data-popup=".main-mnu-wrap">
                            <i class="header-middle__icons-ico">&#xe805</i>
                        </a>
                    </li>
                    <li class="header-middle__icons-item">
                        <a href="javascript:void(0)" class="header-middle__icons-link popup-mobile-btn" data-popup=".header-middle__search">
                            <i class="header-middle__icons-ico">&#xe807</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-md-pull-5 col-sm-4 col-xs-4">
                <a href="/" class="header-middle__logo">
                    <img src="/img/header-logo.png" alt="" class="header-middle__logo-img">
                    <span class="header-middle__logo-text">Автоаксессуары для любых авто</span>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <ul class="header-middle__icons">
                    <li class="header-middle__icons-item">
                        @if($user)
                            <a href="/user#wishlist" class="header-middle__icons-link">
                                <span class="header-middle__icons-count" id="wishlist-count">{!! count($user->wishlist()) !!}</span>
                                <i class="header-middle__icons-ico">&#xe800</i>
                                <span class="header-middle__icons-text">Закладки</span>
                            </a>
                        @else
                            <a href="javascript:void(0)" class="header-middle__icons-link">
                                <span class="header-middle__icons-count">0</span>
                                <i class="header-middle__icons-ico">&#xe800</i>
                                <span class="header-middle__icons-text">Закладки</span>
                            </a>
                            <div class="cart-hover">
                                <span class="cart-hover__text">Войдите, чтобы сохранять понравившиеся Вам товары</span>
                                <a href="/login" class="cart-hover__btn">Войти</a>
                                <span class="cart-hover__text">или</span>
                                <a href="/register" class="cart-hover__cart-link">Зарегистрироваться</a>
                            </div>
                        @endif
                    </li>
                    <li class="header-middle__icons-item">
                        <a href=".cart-popup" class="header-middle__icons-link cart-popup-link">
                            <span class="header-middle__icons-count" id="cart-count">{{ $cart->total_quantity or '0' }}</span>
                            <i class="header-middle__icons-ico">&#xe806</i>
                            <span class="header-middle__icons-text">Корзина</span>
                        </a>
                        <div class="cart-hover">
                            <span class="cart-hover__text">Товаров: <span id="cart-quantity">{{ $cart->total_quantity or '0' }}</span></span>
                            <br>
                            <span class="cart-hover__text">На суммму: <span id="cart-price">{{ $cart->total_price or '0' }}</span> грн</span>
                            <a href="/checkout" class="cart-hover__btn">Оформить заказ</a>
                            <a href=".cart-popup" class="cart-hover__cart-link">Перейти в корзину</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>