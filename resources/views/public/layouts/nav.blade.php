<nav class="main-mnu-wrap">

    <div class="container">
        <ul class="main-mnu">
            @forelse($categories as $category)
                <li class="main-mnu__item">
                    <a href="/categories/{!! $category->url_alias !!}" class="main-mnu__link">{!! $category->name !!}</a>
                </li>
            @empty
            @endforelse
        </ul>
        <ul class="main-mnu main-mnu_mobile">
            <li class="main-mnu__item"><a href="/news" class="main-mnu__link">Новости</a></li>
            @forelse($html_pages as $page)
                <li class="main-mnu__item"><a href="/page/{!! $page->url_alias !!}" class="main-mnu__link">{!! $page->name !!}</a></li>
            @empty
            @endforelse
        </ul>
        <a href="javascript:void(0)" class="close-popup-btn popup-mobile-btn" data-popup=".main-mnu-wrap"><i class="close-popup-btn__icon">&#xe808</i></a>
    </div>
</nav>