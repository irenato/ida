@if ($breadcrumbs)
    <section class="breadcrumbs">
        <div class="container">
            @foreach ($breadcrumbs as $breadcrumb)
                @if (!$breadcrumb->last)
                    <a href="{{ $breadcrumb->url }}" class="breadcrumbs__link">{{ $breadcrumb->title }}</a> >
                @else
                    <span class="breadcrumbs__link breadcrumbs__link_active">{{ $breadcrumb->title }}</span>
                @endif
            @endforeach
        </div>
    </section>
@endif
