<div class="container">
    <div class="row">
        <div class="col-md-2 col-sm-6">
            <a href="/" class="footer__logo">
                <img src="/img/footer-logo.png" alt="" class="header-middle__logo-img">
                <span class="footer__logo-text">Автоаксессуары для любых авто</span>
            </a>
        </div>
        <div class="col-md-3 col-md-push-1 col-sm-6">
            <div class="soc-wrap">
                Присоединяйтесь
                <ul class="soc-list">
                        <li class="soc-item">
                            <a href="{!! $settings->socials->vkontakte or 'javascript:void(0)' !!}" class="soc-item__link"><i class="soc-item__icon">&#xf189</i></a>
                        </li>
                        <li class="soc-item">
                            <a href="{!! $settings->socials->facebook or 'javascript:void(0)' !!}" class="soc-item__link"><i class="soc-item__icon">&#xf09a</i></a>
                        </li>
                        <li class="soc-item">
                            <a href="{!! $settings->socials->youtube or 'javascript:void(0)' !!}" class="soc-item__link"><i class="soc-item__icon">&#xf167</i></a>
                        </li>
                        <li class="soc-item">
                            <a href="{!! $settings->socials->instagram or 'javascript:void(0)' !!}" class="soc-item__link"><i class="soc-item__icon">&#xf16d</i></a>
                        </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2 col-md-push-1 col-sm-4">
            <ul class="footer-mnu">
                <li class="footer-mnu-item"><a href="/news" class="footer-mnu-item__link">Новости</a></li>
                @forelse($html_pages as $page)
                    <li class="footer-mnu-item"><a href="/page/{!! $page->url_alias !!}" class="footer-mnu-item__link">{!! $page->name !!}</a></li>
                @empty
                @endforelse
            </ul>
        </div>
        <div class="col-md-2 col-md-push-1 col-sm-4">
            <ul class="footer-mnu">
                @forelse($categories as $category)
                    @if($category->display_in_footer)
                        <li class="footer-mnu-item">
                            <a href="/categories/{!! $category->url_alias !!}" class="footer-mnu-item__link">{!! $category->name !!}</a>
                        </li>
                    @endif
                @empty
                @endforelse
            </ul>
        </div>
        <div class="col-md-2 col-md-push-1 col-sm-4">
            <ul class="footer-phone-list">
                @foreach($settings->phones as $phone)
                    <li class="footer-phone">{!! $phone !!}</li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="bottom-line clearfix">
        <span class="bottom-line__copyright">© 2016 autoslon.com.ua </span>
        <div class="footer-main__bz-logo">
            <span class="footer-main__bz-title">Разработка сайта:</span>
            <a href="//zavodbiz.com/" target="_blank" class="footer-main__bz-logo-link">Первый бизнес завод</a>
        </div>
    </div>
</div>

<div class="hidden">
    <div class="cart-popup">
        <h2 class="section-title section-title_upper">Корзина</h2>

        <div id="cart_content">
            @include('public.layouts.cart')
        </div>
        

        <div class="cart-result">
            <span class="cart-result__descr">Товаров в корзине: <span class="cart-result__quantity">0</span></span>
            <span class="cart-result__descr">Итого к оплате: <span class="cart-result__price">0</span> грн</span>
            <div class="clearfix cart-result__btn-wrap">
                <a href="/checkout" style="text-align: center" class="cart-result__btn">Оформить заказ</a>
                <a href="javascript:void(0)" class="cart-result__resume">Продолжить покупки</a>
            </div>
        </div>
    </div>
</div>
