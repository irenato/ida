<div class="col-md-3 col-sm-6">
    <div class="articles-item">
        <a href="/news/{!! $article->url_alias !!}" class="articles-item__thumb" style="background-image: url({!! $article->image->get_current_file_url('news_list') !!})"></a>
        <span class="articles-item__date">{!! $article->date !!}</span>
        <a href="/news/{!! $article->url_alias !!}" class="articles-item__title">{!! $article->title !!}</a>
    </div>
</div>