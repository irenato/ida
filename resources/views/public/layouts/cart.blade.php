<section>
    @if(!is_null($cart))
        <form class="cart-form">
            <div class="cart-table-wrap">
                <table class="cart-table">
                    @forelse($cart->get_products() as $product)
                    <tr class="cart-table__row">
                        <td class="cart-table__item cart-table__delete">
                            <a href="javascript:void(0)" class="cart-table__delete-link" data-product-id="{!! $product['product']->id !!}">
                                <i class="header-middle__search-icon cart-table__delete-icon">&#xe808;</i>
                            </a>
                        </td>
                        <td class="cart-table__item cart-table__thumb">
                            <a href="/product/{!! $product['product']->url_alias !!}" class="cart-table__thumb-link" style="background-image: url('{!! $product['product']->image->get_current_file_url() !!}') ">
                            </a>
                        </td>
                        <td class="cart-table__item cart-table__name">
                            <a href="/product/{!! $product['product']->url_alias !!}" class="cart-table__name-link">{!! $product['product']->name !!}</a>
                        </td>
                        <td class="cart-table__item cart-table__quantity">
                            <div class="product-short-info__amount">
                                <span class="product-short-info__amount-inc product-short-info__amount-inc_minus"><i class="product-short-info__amount-icon">&#xe80e</i></span>
                                <input type="text" class="product-short-info__amount-input count_field" data-product-id="{!! $product['product']->id !!}" value="{!! $product['quantity'] !!}" readonly>
                                <span class="product-short-info__amount-inc product-short-info__amount-inc_plus"><i class="product-short-info__amount-icon">&#xe80d</i></span>
                            </div>
                        </td>
                        <td class="cart-table__item cart-table__price">{!! $product['product']->price * $product['quantity'] !!} грн</td>
                    </tr>
                    @empty
                        <tr class="cart-table__row">
                            <td align="center">Ваша корзина пуста!</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </form>
    @else
        <div class="title-wrap">
            <h2 class="section-title">Ваша корзина пуста!</h2>
        </div>
    @endif

    <input type="hidden" id="total_quantity" value="{!! $cart->total_quantity or '0' !!}">
    <input type="hidden" id="total_price" value="{!! $cart->total_price or '0' !!}">

</section>