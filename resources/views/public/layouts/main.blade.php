<!DOCTYPE html>
<html>
@include('public.layouts.header')

<body class="account-body{{ Request::path()=='/' ? ' home' : '' }}">

<main class="wrapper">
    <section class="content">
        <header class="main-header">
            @include('public.layouts.header-main')
            @include('public.layouts.header-middle')
            @include('public.layouts.nav')
        </header>
        @yield('breadcrumbs')
        @yield('content')
    </section>
    <footer class="main-footer">
        @include('public.layouts.footer')
    </footer>
</main>
@include('public.layouts.footer-scripts')
</body>
</html>