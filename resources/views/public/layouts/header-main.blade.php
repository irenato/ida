<section class="top-line">
    <div class="container">
        <ul class="top-line__mnu top-line__mnu_left">
            @forelse($html_pages as $page)
                <li class="top-line__mnu-item"><a href="/page/{!! $page->url_alias !!}" class="top-line__mnu-link">{!! $page->name !!}</a></li>
            @empty
            @endforelse
            <li class="top-line__mnu-item"><a href="/news" class="top-line__mnu-link">Новости</a></li>
        </ul>

        <ul class="top-line__mnu top-line__mnu_right">
            @if($user)
                <li class="top-line__mnu-item"><a href="/user" class="top-line__mnu-link">Личный кабинет</a></li>
                <li class="top-line__mnu-item">/</li>
                <li class="top-line__mnu-item"><a href="/logout" class="top-line__mnu-link">Выход</a></li>
            @else
                <li class="top-line__mnu-item"><a href="/login" class="top-line__mnu-link">Вход</a></li>
                <li class="top-line__mnu-item">/</li>
                <li class="top-line__mnu-item"><a href="/register" class="top-line__mnu-link">Регистрация</a></li>
            @endif
        </ul>

        <ul class="top-line__mnu top-line__mnu_right top-line__mnu_mobile">
            @if($user)
                <li class="top-line__mnu-item"><a href="/user" class="top-line__mnu-link"><i class="header-middle__login-ico">&#xe803</i></a></li>
                <li class="top-line__mnu-item"><a href="/logout" class="top-line__mnu-link"><i class="header-middle__login-ico">&#xe802</i></a></li>
            @else
                <li class="top-line__mnu-item"><a href="/login" class="top-line__mnu-link"><i class="header-middle__login-ico">&#xe803</i></a></li>
                <li class="top-line__mnu-item"><a href="/register" class="top-line__mnu-link"><i class="header-middle__login-ico">&#xe802</i></a></li>
            @endif
        </ul>

        <ul class="header-middle__phones header-middle__phones_mobile clearfix">
            @forelse($settings->phones as $i => $phone)
                <li class="header-middle__phones-item">
                    {!! $i === 0 ? '<i class="header-middle__phones-ico">&#xe804</i>' : '' !!}
                    {!! $phone !!}
                </li>
            @empty
            @endforelse
        </ul>
    </div>
</section>