@extends('public.layouts.main')

@section('meta')
    <title>Вход в личный кабинет</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('content')
    <section class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-push-1 col-sm-6">
                    <h3 class="inner-title">Вход в личный кабинет</h3>
                    @if(!empty($errors->all()))
                        <div class="error-message">
                            {!! $errors->first() !!}
                        </div>
                    @endif
                    <form class="login-form" method="post">
                        {!! csrf_field() !!}
                        <div class="clearfix">
                            <label class="login-form__label" for="login-name">E-mail:</label>
                            <input type="text"
                                   id="login-name"
                                   name="email"
                                   class="login-form__input @if($errors->has('email')) input-error @endif"
                                   value="{!! old('email') !!}"
                            />
                        </div>
                        <div class="clearfix">
                            <label class="login-form__label" for="login-password">Пароль:</label>
                            <input type="password" name="password" id="login-password" class="login-form__input">
                        </div>
                        <a href="/forgotten" class="login-form__forgot">Забыли пароль?</a><br>
                        <button type="submit" class="login-form__btn">Войти</button>
                    </form>
                </div>
                <div class="col-md-5 col-md-push-2 col-sm-6">
                    <div class="reg-advantages">
                        <h3 class="inner-title inner-title_small">Регистрируйтесь и получайте преимущества</h3>
                        <ul class="reg-advantages__list">
                            <li class="reg-advantages__list-item">С легкостью следите за статусом заказов</li>
                            <li class="reg-advantages__list-item">Мы будем держать Вас в курсе о новинках и акциях</li>
                            <li class="reg-advantages__list-item">Получите уникальные скидки и предложения</li>
                        </ul>
                        <a href="/register" class="reg-advantages__btn">Зарегистрироваться</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection