@extends('public.layouts.main')
@section('meta')
    <title>Ошибка 404. Страница не найдена</title>
@endsection
@section('content')
    <section class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="thx-text">
                        <img src="/img/404.png" alt="" class="img-404">
                        <a href="/" class="continue-btn">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection