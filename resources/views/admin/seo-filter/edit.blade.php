@include('admin.layouts.header')
@include('admin.layouts.header')
@extends('admin.layouts.main')
@section('title')
    Добавление страницы
@endsection
@section('content')

    <div class="content-title">
        <div class="row">
            <div class="col-sm-12">
                <h1>Добавление seo данных</h1>
            </div>
        </div>
    </div>

    @if (session('message-success'))
        <div class="alert alert-success">
            {{ session('message-success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif(session('message-error'))
        <div class="alert alert-danger">
            {{ session('message-error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="form">
        <form method="post" action="{{ url('admin/seo-filter/edit/' . $seo->id) }}">
            {!! csrf_field() !!}
            <div class="panel-group">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>SEO</h4>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right control-label">Url</label>
                                <div class="form-element col-sm-10">
                                    <input type="text" data-translit="output" class="form-control" name="url" value="{!! $seo->url !!}" />
                                    @if($errors->has('url'))
                                        <p class="warning" role="alert">{!! $errors->first('url',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Meta title</label>
                                <div class="form-element col-sm-10">
                                    <input type="text" class="form-control" name="seo_title" value="{!! $seo->seo_title !!}" />
                                    @if($errors->has('seo_title'))
                                        <p class="warning" role="alert">{!! $errors->first('seo_title',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Meta description</label>
                                <div class="form-element col-sm-10">
                                    <textarea name="seo_desc" class="form-control" rows="6">{!! $seo->seo_desc !!}</textarea>
                                    @if($errors->has('seo_desc'))
                                        <p class="warning" role="alert">{!! $errors->first('seo_desc',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Meta keywords</label>
                                <div class="form-element col-sm-10">
                                    <textarea name="seo_key" class="form-control" rows="6">{!! $seo->seo_key !!}</textarea>
                                    @if($errors->has('seo_key'))
                                        <p class="warning" role="alert">{!! $errors->first('seo_key',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Meta robots</label>
                                <div class="form-element col-sm-10">
                                    <input type="text" class="form-control" name="seo_robots" value="{!! $seo->seo_robots !!}" />
                                    @if($errors->has('seo_robots'))
                                        <p class="warning" role="alert">{!! $errors->first('seo_robots',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Meta canonical</label>
                                <div class="form-element col-sm-10">
                                    <input type="text" class="form-control" name="seo_canonical" value="{!! $seo->seo_canonical !!}" />
                                    @if($errors->has('seo_canonical'))
                                        <p class="warning" role="alert">{!! $errors->first('seo_canonical',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Meta content</label>
                                <div class="form-element col-sm-10">
                                    <textarea name="content" class="form-control" rows="6">{!! $seo->content !!}</textarea>
                                    @if($errors->has('content'))
                                        <p class="warning" role="alert">{!! $errors->first('content',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-10 col-sm-push-2 text-left">
                                    <button type="submit" class="btn">Сохранить</button>
                                    <a href="{{ url('/admin/seo-filter') }}" class="btn btn-primary">Назад</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <script src="/js/libs/transliterate.js"></script>
@endsection
