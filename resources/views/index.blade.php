@extends('public.layouts.main')
@section('meta')
    <title>{!! $settings->meta_title !!}</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('content')

    @if(!empty($popular_categories))
        <section class="popular">
            <div class="container">
                <div class="title-wrap">
                    <h2 class="section-title">Популярные категории</h2>
                </div>
                <div class="row">
                    <div class="@if (count($popular_categories) <=3) col-md-12 @else col-md-8 @endif">
                        @foreach($popular_categories as $i => $category)
                            @if($i <= 2)
                                <div class="@if($i == 2 || (count($popular_categories) == 1)) col-md-12 @else col-md-6 @endif col-sm-6">
                                    <a href="/categories/{!! $category->url_alias !!}" class="popular-item popular-item_low">
                                        <div class="popular-item__img popular-item__img_low" style="background-image: url({!! $category->image->get_current_file_url() !!});">
                                        </div>
                                        <div class="popular-item__descr">
                                            <span class="popular-item__title">{!! $category->name !!}</span>
                                        </div>
                                    </a>
                                </div>
                            @elseif($i == 3)
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <a href="/categories/{!! $category->url_alias !!}" class="popular-item popular-item_high">
                                        <div class="popular-item__img popular-item__img_high" style="background-image: url({!! $category->image->get_current_file_url() !!});">
                                        </div>
                                        <div class="popular-item__descr">
                                            <span class="popular-item__title">{!! $category->name !!}</span>
                                        </div>
                                    </a>
                                </div>
                            @else
                                </div>
                            @endif
                        @break($i == 3)
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if(!$latest_products->isEmpty())
    <section class="new">
        <div class="container">
            <div class="title-wrap">
                <h2 class="section-title">Новинки</h2>
            </div>
            <div class="product-slider">
                @foreach($latest_products as $product)
                    @include('public.layouts.product', ['product' => $product, 'slider' => true])
                @endforeach
            </div>
        </div>
    </section>
    @endif

    @if(!$bestseller_products->isEmpty())
    <section class="leaders">
        <div class="container">
            <div class="title-wrap">
                <h2 class="section-title">Лидеры продаж</h2>
            </div>
            <div class="product-slider">
                @foreach($bestseller_products as $product)
                    @include('public.layouts.product', ['product' => $product, 'slider' => true])
                @endforeach
            </div>
        </div>
    </section>
    @endif

    @if(!$news->isEmpty())
    <section class="articles">
        <div class="container">
            <div class="title-wrap">
                <h2 class="section-title">Новости</h2>
            </div>
            <div class="row">
                @foreach($news as $article)
                    @include('public.layouts.news', ['article' => $article])
                @endforeach
            </div>
        </div>
    </section>
    @endif

@endsection