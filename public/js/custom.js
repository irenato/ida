jQuery(document).ready(function($){

    $('input[name="phone"]').mask('+38(099)999-99-99');

    /**
     * Добавление и удаление товаров из списка закладок
     */
    $('.button-wishlist').on('click', function () {
        var data = {
            product_id: $(this).attr('data-product-id'),
            action: $(this).attr('data-action')
        };
        var $this = $(this);
        var all = $('.button-wishlist');

        $.post('/wishlist/update', data, function(response) {
            if (response.count !== false) {
                $('#wishlist-count').html(response.count);

                var segments = location.pathname.split('/');
                if(segments[1] == 'user'){
                   $this.parent().parent().fadeOut(500, function(){
                       $this.parent().parent().remove();
                    });

                    setTimeout(function(){
                        if (response.count == 0) {
                            $('.wishlist-error').removeClass('hidden');
                        }
                    },500);

                }

                if (response.action == 'add') {console.log('add');console.log(all);
                    $.each(all, function(i, attributes){
                        if($(attributes).attr('data-product-id') == data.product_id) {
                            $(attributes).find('i.close-popup-btn__fav-icon').html('&#xe801;');
                            $(attributes).attr('data-action', 'remove');
                        }
                    });
                } else if (response.action == 'remove') {console.log('remove');console.log(all.attr('data-product-id'));
                    $.each(all, function(i, attributes){
                        if($(attributes).attr('data-product-id') == data.product_id) {
                            $(attributes).find('i.close-popup-btn__fav-icon').html('&#xe800;');
                            $(attributes).attr('data-action', 'add');
                        }
                    });
                }
            }

            if (response.error) {
                if (response.error == 'unregistered') {
                    $this.next('.wishlist-error').addClass('active');
                }
            }
        });

    });

    $('.wishlist-error').on('mouseleave', function() {
        $(this).removeClass('active');
    });

    /*
     * Добавление отзывов и комментариев к ним
     */

    /**
     * Попап для незарегистрированных пользователей о запрете голосования
     */
    $('#add-like, #add-dislike').on('mouseenter', function () {
        $('.cart-hover').removeClass('active');
        if($(this).hasClass('unregistered')){
            $(this).parent().parent().find('.cart-hover').addClass('active');
        }
    });

    /*
     * Добавление отзывов комментариев
     */
    $('form.review-form, form.answer-form').on('submit', function(e){
        e.preventDefault();
        var $this = $(this);

        $.ajax({
            url: '/review/add',
            data: $(this).serialize(),
            method: 'post',
            dataType: 'json',
            beforeSend: function() {
                $this.find('.error-message').fadeOut(300);
                $this.find('button[type="submit"]').html('Отправляем...');
            },
            success: function (response) {
                if(response.error){
                    var html = '';
                    $.each(response.error, function(i, value){
                        html += value + '<br>';
                    });
                    $('#error-' + response.type + ' > div').html(html);
                    $('#error-' + response.type).fadeIn(300);
                } else if(response.success) {
                    $('#error-' + response.type + ' > div').html(response.success);
                    $('#error-' + response.type).fadeIn(300);

                    setTimeout(function(){
                        $this.slideUp('slow');
                        $('.review-btn').fadeIn('slow');
                    },2500);
                    $('form.' + response.type + '-form')[0].reset();
                }
                $this.find('button[type="submit"]').html('Оставить отзыв')
            }
        });
    });

    /**
     * Обработка лайков и дизлайков
     */
    $('#add-like, #add-dislike').on('click', function(){
        var $this = $(this);

        var data = {
            '_token': $('input[name="_token"]').val(),
            'review_id': $(this).data('review'),
            'action': $(this).data('action')
        };

        $.ajax({
            url: '/review/add-likes',
            data: data,
            method: 'post',
            dataType: 'json',
            beforeSend: function() {
                $(this).find('.error-message').fadeOut(300);
            },
            success: function (response) {
                if(response.error){
                    var html = '<div class="cart-hover usefull-hover active error" onmouseleave="$(this).remove()">';
                    html += '<span class="cart-hover__text">Ошибка: ' + response.error + '</span>';
                    html += '</div>';

                    $this.parent().parent().append(html);
                } else if(response.success) {
                    var html = '<div class="cart-hover usefull-hover active success" onmouseleave="$(this).remove()">';
                    html += '<span class="cart-hover__text">' + response.success + '</span>';
                    html += '</div>';

                    $this.parent().parent().find('#add-like').html('Да('+ response.like +')');
                    $this.parent().parent().find('#add-dislike').html('Нет('+ response.dislike +')');

                    $this.parent().parent().append(html);
                }
            }
        });
    });

    /**
     * Фильтрация товаров в категории
     */
    $('form.product-filter-form').on('submit', function(e){
        var sort = $('select[name="sorting"]').val();
        console.log(sort);
        if (typeof sort === 'undefined' || !sort){
            sort = 'popularity';
        }
        $(this).append('<input type="hidden" name="sort" value="' + sort + '" />');
        return true;
    });

    $('#min-price, #max-price, input[name="filters[]').on('change', function(){
        $('.filters-submit').remove();

        var path = location.pathname;
        var $this = $(this).parent();
        var filters = [];

        $('input[name="filters[]"]').each(function(){
            if($(this).prop('checked')){
                filters.push($(this).val());
            }
        });

        var data = {
            prices: {
                min: $('#min-price').val(),
                max: $('#max-price').val()
            },
            filters: filters,
            json: true
        };
        
        $.ajax({
            url: path,
            data: data,
            dataType: 'json',
            type: 'get',
            success: function(response, e) {
                var html = '<div class="filters-submit">';
                html += '<span class="filters-submit__text">Выбрано: <b>' + response + '</b></span>';
                html += '<button type="submit" class="catalog-content__btn">Подобрать</button></div>';

                $this.append(html);
                $this.find('.filters-submit').fadeIn(300);
            }
        });
    });

    /**
     * Отображение списка закладок пользователя при входе в личный кабинет
     * (по ссылке из шапки страницы)
     */
    if(location.pathname == '/user') {
        if (location.hash == '#wishlist'){
            $('ul.tabs-caption li, article.tabs-content').removeClass('active');
            $('li[data-action="wishlist"], article[data-action="wishlist"]').addClass('active');
        }
    }

    /**
     * Отображение таба "Я постоянный клиент" в чекауте
     * (в случае ошибки ввода логина или пароля)
     */
    if(location.pathname == '/user') {
        if (location.hash == '#login'){
            $('ul.tabs-caption li, article.tabs-content').removeClass('active');
            $('li[data-action="wishlist"], article[data-action="wishlist"]').addClass('active');
        }
    }

    /**
     * Добавление товара в корзину
     */
    $('.btn_buy').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var data = {
            action: 'add',
            product_id: $(this).data('product-id')
        };

        if ($('#product-card-quantity').length){
            data.quantity = $('#product-card-quantity').val();
        }

        update_cart(data);
    });

    /**
     * Удаление товара из корзины
     */
    $('#cart_content, #order_cart_content').on('click', '.cart-table__delete-link', function(){
        var $this = $(this);
        update_cart({
            action: 'remove',
            product_id: $this.data('product-id')
        });
    });

    /**
     * Обновление количества товара в корзине
     */
    $('#cart_content, #order_cart_content').on('input change', '.count_field', function(){
        var $this = $(this);
        update_cart({
            action: 'update',
            product_id: $this.data('product-id'),
            quantity: $this.val()
        });
    });

    /**
     * Обновление корзины
     * @param data
     */
    function update_cart(data){

        $("#cart_content").load("/cart/update", data, function(cart){
            var order_cart_content = $('#order_cart_content');

            if(order_cart_content.length > 0){
                order_cart_content.html(cart);
            }

            var quantity = $('#total_quantity').val();
            var price = $('#total_price').val();

            $('#cart-count, #cart-quantity, .cart-result__quantity').html(quantity);
            $('#cart-price, .cart-result__price').html(price);

            if(order_cart_content.length > 0){
                $('#order_cart-price').html(price);
            } else {
                $.magnificPopup.open({
                    items: {
                        src: $('.cart-popup')
                    },
                    type: 'inline'
                });
            }
        });
    }

    /**
     * Оформление заказа
     */

    /**
     * Обработка первого шага оформления заказа
     */
    $('#checkout-personal-info').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var error_div = form.find('.error-message');
        var error_text = error_div.find('.error-message__text');

        $.ajax({
            url: '/order/create',
            type: 'post',
            data: $(this).serialize(),
            beforeSend: function() {
                $('.checkout-step__body').addClass('checkout-step__body_loader');
                error_div.fadeOut(300);
                error_text.html('');
                form.find('input').removeClass('input-error');

            },
            success: function(response) {
                if(response.error){
                    var html = '';
                    $.each(response.error, function(field, error){
                        html += error + '<br>';
                        form.find('input[name="' + field + '"]').addClass('input-error');
                    });
                    form.find('.error-message__text').html(html);
                    $('.checkout-step__body').removeClass('checkout-step__body_loader');
                    form.find('.error-message').fadeIn(300);
                } else if(response.success){
                    $('.checkout-step__body').removeClass('checkout-step__body_loader');
                    $('.checkout-step__body_first').slideUp('slow');
                    $('.checkout-step__body_second').slideDown('slow');
                    $('.checkout-step__edit').toggleClass('hidden');
                    $('#current_order_id, #current_order_id_2').val(response.success);
                    $('#checkout-step__region').trigger('refresh');
                }
            }
        })
    });

    /**
     * Отображение полей в зависимости от выбранного способа доставки
     */
    $('.checkout-step__body_second').on('change', '#checkout-step__delivery', function(){
        if ($(this).val() != 0) {
            $('.checkout-step__body').addClass('checkout-step__body_loader');
            $('.checkout-step__body_second .error-message').fadeOut(300);
            $('.checkout-step__body_second .error-message__text').html('');
            var data = {
                delivery: $(this).val(),
                order_id: $('#current_order_id').val()
            };

            $("#checkout-delivery-payment").load("/checkout/delivery", data, function (cart) {
                $('select').styler();
            });
            $('.checkout-step__body').removeClass('checkout-step__body_loader');
        }
    });

    /**
     * Обработка второго шага оформления заказа
     */
    $('#checkout-step-2').on('submit', function(e){
        e.preventDefault();

        $.ajax({
            url: '/checkout/confirm',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend: function(){
                $('.checkout-step__body').addClass('checkout-step__body_loader');
                $('.checkout-step__body_second .error-message').fadeOut(300, function(){
                    $('.checkout-step__body_second .error-message__text').html('');
                });
                $('select, input').removeClass('input-error');
            },
            success: function(response) {

                if (response.error) {
                    var html = '';
                    $.each(response.error, function (id, text){
                        console.log(id);
                        var error = id.split('.');
                        $('[name="' + error[0] + '[' + error[1] + ']"').addClass('input-error');
                        html += text + '<br>';
                    });
                    $('.checkout-step__body_second .error-message__text').html(html);
                    $('.checkout-step__body').removeClass('checkout-step__body_loader');
                    $('.checkout-step__body_second .error-message').fadeIn(300);
                } else if (response.success) {
                    if (response.success == 'redirect') {
                        window.location = '/checkout/complete?order_id=' + response.order_id;
                    }
                }
            }
        })
    });
});

/**
 * Загрузка городов и отделений Новой Почты
 * @param id
 * @param value
 */
function newpostUpdate(id, value) {
    if (id == 'city') {
        var data = {
            city_id: value
        };
        var path = '/checkout/warehouses';
        var selector = $('#checkout-step__warehouse');
    } else if (id == 'region') {
        var data = {
            region_id: value
        };
        var path = 'checkout/cities';
        var selector = $('#checkout-step__city');
    }

    $.ajax({
        url: path,
        data: data,
        type: 'post',
        dataType: 'json',
        beforeSend: function() {
            $('.checkout-step__body_second .error-message').fadeOut(300);
            $('.checkout-step__body').addClass('checkout-step__body_loader');
            $('.checkout-step__body_second .error-message__text').html('');
            $('#checkout-step__warehouse').html('<option value="0">Сначала выберите город!</option>');
            $('#checkout-step__warehouse').trigger('refresh');
        },
        success: function(response){
            if (response.error) {
                $('.checkout-step__body_second .error-message__text').html(response.error);
                $('.checkout-step__body').removeClass('checkout-step__body_loader');
                $('.checkout-step__body_second .error-message').fadeIn(300);
            } else if (response.success) {
                var html = '<option value="0">Выберите...</option>';
                $.each(response.success, function(i, resp){
                    if (id == 'city') {
                        var info = resp.address_ru;
                    } else if (id == 'region') {
                        var info = resp.name_ru;
                    }
                    html += '<option value="' + resp.id + '">' + info + '</option>';
                });
                selector.html(html);
                selector.trigger('refresh');
                $('.checkout-step__body').removeClass('checkout-step__body_loader');
            }
        }
    })
}

/**
 * Сортировка товаров в категории
 * @param sort
 */
function sortBy(sort){
    var locate = location.search.split('&');
    var new_location = '';

    $.each(locate, function (i, value) {
        var parameters = value.split('=');
        if (parameters[0] != 'sort') {
            new_location += value + '&';
        }
    });

    location.search = new_location + 'sort=' + sort;
}

/**
 * Подписка на новости
 */
$('.footer-top__form').on('submit', function(e){
    e.preventDefault();

    $.ajax({
        url: '/subscribe',
        data: $(this).serialize(),
        method: 'post',
        dataType: 'json',
        success: function(response){
            if (response.email){
                $('.del-info-popup .del-info-popup__text').html(response.email);
            } else if (response.success) {
                $('.del-info-popup .del-info-popup__text').html(response.success);
            }

            $.magnificPopup.open({
                items: {
                    src: $('.del-info-popup')[0].outerHTML
                },
                type: 'inline'
            });

            $('.footer-top__form').find('input[type="email"]').val('');
        }
    });
});

/**
 * Редактирование данных в личном кабинете
 */
$('#personal-account-info').on('submit', function(e) {
    e.preventDefault();
    var form = $(this);
    var error_div = form.find('.error-message');
    var error_text = error_div.find('.error-message__text');
    var button = form.find('button[type="submit"]');
    var inputs = form.find('input');

    $.ajax({
        url: '/user/change-data',
        type: 'post',
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function() {
            error_div.addClass('hidden');
            error_text.html('');
            inputs.removeClass('input-error');
            button.html('Отправляем...')
        },
        success: function(response) {
            if (response.error) {
                $.each(response.error, function(field, error){
                    error_text.append(error + '<br>');
                    form.find('input[name="' + field + '"]').addClass('input-error');
                });
                error_div.removeClass('hidden');
                button.html('Сохранить');
            } else if (response.success) {
                $.each(response.success, function (field, value){
                    form.find('input[name="' + field + '"]').val(value);
                });
                error_text.append('Ваши данные успешно сохранены!');
                error_div.removeClass('hidden');
                button.html('Сохранить');
            }
        }
    });
});