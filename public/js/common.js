$(function() {

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

	$('.popup-mobile-btn').click(function(){
		var popupClass = $(this).attr('data-popup');
		$(popupClass).toggleClass('active');
	});

	$('.product-slider').slick({
		infinite: true,
		arrows: true,
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		accessibility: false,
		responsive: [
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3
			}
		},
		{
			breakpoint: 980,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 1
			}
		}
		]
	});

	$('.cart-popup-link, .cart-hover__cart-link').magnificPopup({
		type:"inline"
	});	

	$('.product-thumb__link').magnificPopup({
		type:"image"
	});

	$('#cart_content').on('click', '.cart-result__resume', function(){
		$.magnificPopup.close();
	});

	$('ul.tabs-caption').on('click', '.tabs-caption__item:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.tabs').find('.tabs-content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.product-tabs-caption').on('click', '.product-tabs-caption__item:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.product-tabs').find('.product-tabs-content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('.product-thumb__slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product-thumb__slider-nav'
	});

	$('.product-thumb__slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.product-thumb__slider',
		dots: false,
		focusOnSelect: true,
		responsive: [
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3
			}
		}
		]
	});

	$('#cart_content, .product-short-info, #order_cart_content').on('click', '.product-short-info__amount-inc', function(){
		var field = $(this).siblings('.product-short-info__amount-input');
		var testValue = field.val();
		if($(this).hasClass('product-short-info__amount-inc_minus')){
			testValue--;
			if(testValue == 0){
				testValue = 0;
			}
		}	
		else{
			testValue++;
		}

		if(testValue != 0)
			field.val(testValue).trigger('change');
	});

	var minPrice = $('#min-price');
	var maxPrice = $('#max-price');

	$( "#slider-range" ).slider({
		range: true,
		values: [minPrice.val(), maxPrice.val()],
		slide: function( event, ui ) {
			$( "#amount" ).val(ui.values[ 0 ] + " грн - " + ui.values[ 1 ] + " грн");
		},
		stop: function (event, ui) {
			if(minPrice.val() != ui.values[0]) {
				minPrice.val(ui.values[0]).trigger('change');
			}
			if(maxPrice.val() != ui.values[1]) {
				maxPrice.val(ui.values[1]).trigger('change');
			}
		}
	}).slider( "option", "min", parseInt($("#slider-range").attr('data-min'))).slider("option", "max", parseInt($( "#slider-range" ).attr('data-max')) );

	$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + " грн - " + $( "#slider-range" ).slider( "values", 1 ) + " грн");

	$('select').styler();

	$('.filter-check-title').click(function(){
		$(this).closest('.filter-check-list-wrap').addClass('filter-check-list-wrap_active');
		$('.filter-check-list-wrap_active .filter-check-list').slideToggle();
		$('.filter-check-list-wrap_active .filter-check-title__icon').toggleClass('filter-check-title__icon_active');
		$('.filter-check-list-wrap_active').removeClass('filter-check-list-wrap_active');
	});

	$('.checkout-form__radio').change(function(){
		$('.checkout-form__password').slideToggle('hidden');
	});

	$('.checkout-step__edit').click(function(){
		$('.checkout-step__body_second').slideUp('slow');
		$('.checkout-step__body_first').slideDown('slow');
		$(this).toggleClass('hidden');
	});

	$('#review-slide').click(function(){
		$(this).fadeOut('fast');
		$('.review-form').slideDown('slow');
	});	

	$('.review-form__btn_cancel').click(function(){
		$('.review-form').slideUp('slow');
		$('.review-btn').fadeIn('slow');
	});

	$('.all-review-btn').click(function(){
		$('.review-item').show();
		$(this).hide();
	});

	$('#comment-slide').click(function(){
		$(this).siblings('.answer-form').slideDown('slow');
	});

	$('.answer-form__btn_cancel').click(function(){
		$(this).parents('.answer-form').slideUp('slow');
	});

});
