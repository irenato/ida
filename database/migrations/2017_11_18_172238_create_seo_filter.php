<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoFilter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('seo_title');
            $table->string('seo_desc');
            $table->string('seo_key');
            $table->string('seo_robots');
            $table->string('seo_canonical');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo_filters');
    }
}
