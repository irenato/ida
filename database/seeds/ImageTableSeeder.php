<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            [
                'href' => 'no_image.jpg',
                'title' => 'Изображение не выбрано',
                'type' => 'main',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'xJZLR4X019.jpeg',
                'title' => '_article-1.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'MaWIr4yTCC.jpeg',
                'title' => '_popular_1.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'rAo9GQQOhY.jpeg',
                'title' => '_popular_2.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'xrBawA4kSs.jpeg',
                'title' => '_popular_3.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'YMIyAZyCp8.jpeg',
                'title' => '_popular_4.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'y9gWAqjRaE.jpeg',
                'title' => '_product-1.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'Tbt1YUoGPY.jpeg',
                'title' => '_product-2.jpg'
                ,'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => '4LeMFE9ic4.jpeg',
                'title' => '_product-3.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'Fmk3PB8IJ6.jpeg',
                'title' => '_product-4.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'href' => 'xjsl4Z0VG8.jpeg',
                'title' => '_product-5.jpg',
                'type' => '',
                'sizes' => '[]',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}

