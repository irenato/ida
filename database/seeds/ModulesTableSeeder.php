<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'name'              => 'Слайдшоу на главной',
                'alias_name'        => 'slideshow',
                'status'            => 1,
                'settings'          => json_encode([
                    'slides' => [
                        [
                            'image_id'  => 1,
                            'link'      => '/',
                            'enable'    => 1,
                            'sort_order' => 1,
                        ]
                    ],
                    'quantity'  => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Новинки товаров',
                'alias_name'        => 'latest',
                'status'            => 1,
                'settings'          => json_encode(['products' => [1], 'quantity' => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Лидеры продаж',
                'alias_name'        => 'bestsellers',
                'status'            => 1,
                'settings'          => json_encode(['products' => [1], 'quantity' => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ]);
    }
}
