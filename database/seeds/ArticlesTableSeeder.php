<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ArticlesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('articles')->insert([
            [
                'user_id' => 1,
                'url_alias' => 'kak-vybrat-kovriki-v-salon-i-ostatsya-dovolnym-svoim-vyborom',
                'title' => 'Как выбрать коврики в салон и остаться довольным своим выбором',
                'subtitle' => 'Коврики в салон',
                'text' => '&lt;p&gt;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&lt;/p&gt;
',
                'image_id' => 2,
                'meta_title' => 'Коврики в салон',
                'meta_keywords' => '',
                'meta_description' => '',
                'published' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => 1,
                'url_alias' => 'kak-vybrat-deflectory-dlya-okon-i-ostatsya-dovolnym-svoim-vyborom',
                'title' => 'Как выбрать дефлекторы для окон и остаться довольным своим выбором',
                'subtitle' => 'Дефлекторы для окон',
                'text' => '&lt;p&gt;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&lt;/p&gt;
',
                'image_id' => 2,
                'meta_title' => 'Дефлекторы для окон',
                'meta_keywords' => '',
                'meta_description' => '',
                'published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => 1,
                'url_alias' => 'kak-vybrat-deflectory-kapota-i-ostatsya-dovolnym-svoim-vyborom',
                'title' => 'Как выбрать дефлекторы капота и остаться довольным своим выбором',
                'subtitle' => 'Дефлекторы капота',
                'text' => '&lt;p&gt;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&lt;/p&gt;
',
                'image_id' => 2,
                'meta_title' => 'Дефлекторы капоты',
                'meta_keywords' => '',
                'meta_description' => '',
                'published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => 1,
                'url_alias' => 'kak-vybrat-chehly-i-ostatsya-dovolnym-svoim-vyborom',
                'title' => 'Как выбрать чехлы в салон и остаться довольным своим выбором',
                'subtitle' => 'Чехлы в салон',
                'text' => '&lt;p&gt;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&lt;/p&gt;
',
                'image_id' => 2,
                'meta_title' => 'Чехлы в салон',
                'meta_keywords' => '',
                'meta_description' => '',
                'published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => 2,
                'url_alias' => 'kak-vybrat-aksesyaru-i-ostatsya-dovolnym-svoim-vyborom',
                'title' => 'Как выбрать аксессуары и остаться довольным своим выбором',
                'subtitle' => 'Аксессуары',
                'text' => '&lt;p&gt;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&amp;nbsp;Коврики в салон&lt;/p&gt;
',
                'image_id' => 2,
                'meta_title' => 'Аксессуары',
                'meta_keywords' => '',
                'meta_description' => '',
                'published' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
