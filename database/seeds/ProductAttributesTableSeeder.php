<?php

use Illuminate\Database\Seeder;

class ProductAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_attributes')->insert([
            [
                'product_id'            => 1,
                'attribute_id'          => 1,
                'attribute_value_id'    => 1
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 2,
                'attribute_value_id'    => 4
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 3,
                'attribute_value_id'    => 7
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 4,
                'attribute_value_id'    => 10
            ],
            [
                'product_id'            => 2,
                'attribute_id'          => 1,
                'attribute_value_id'    => 2
            ],
            [
                'product_id'            => 2,
                'attribute_id'          => 2,
                'attribute_value_id'    => 5
            ],
            [
                'product_id'            => 2,
                'attribute_id'          => 3,
                'attribute_value_id'    => 8
            ],
            [
                'product_id'            => 2,
                'attribute_id'          => 4,
                'attribute_value_id'    => 11
            ],
            [
                'product_id'            => 3,
                'attribute_id'          => 1,
                'attribute_value_id'    => 3
            ],
            [
                'product_id'            => 3,
                'attribute_id'          => 2,
                'attribute_value_id'    => 6
            ],
            [
                'product_id'            => 3,
                'attribute_id'          => 3,
                'attribute_value_id'    => 9
            ],
            [
                'product_id'            => 3,
                'attribute_id'          => 4,
                'attribute_value_id'    => 12
            ],
            [
                'product_id'            => 4,
                'attribute_id'          => 1,
                'attribute_value_id'    => 2
            ],
            [
                'product_id'            => 4,
                'attribute_id'          => 2,
                'attribute_value_id'    => 6
            ],
            [
                'product_id'            => 4,
                'attribute_id'          => 3,
                'attribute_value_id'    => 8
            ],
            [
                'product_id'            => 4,
                'attribute_id'          => 4,
                'attribute_value_id'    => 10
            ],
        ]);
    }
}
