<?php

use Illuminate\Database\Seeder;

class AttributeValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attribute_values')->insert([
            [
                'attribute_id' => 1,
                'name' => 'AutoMir'
            ],
            [
                'attribute_id' => 1,
                'name' => 'EMC'
            ],
            [
                'attribute_id' => 1,
                'name' => 'MW Brothers'
            ],
            [
                'attribute_id' => 2,
                'name' => 'На крышу'
            ],
            [
                'attribute_id' => 2,
                'name' => 'На багажник'
            ],
            [
                'attribute_id' => 2,
                'name' => 'На задний бампер'
            ],
            [
                'attribute_id' => 3,
                'name' => 'Кожа'
            ],
            [
                'attribute_id' => 3,
                'name' => 'Ткань'
            ],
            [
                'attribute_id' => 3,
                'name' => 'Велюр'
            ],
            [
                'attribute_id' => 4,
                'name' => 'Черный'
            ],
            [
                'attribute_id' => 4,
                'name' => 'Серый'
            ],
            [
                'attribute_id' => 4,
                'name' => 'Бежевый'
            ],
        ]);
    }
}