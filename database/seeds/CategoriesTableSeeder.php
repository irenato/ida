<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'              => 'Авточехлы',
                'description'       => null,
                'image_id'          => 3,
                'meta_title'        => 'Авточехлы',
                'meta_description'  => 'Авточехлы',
                'meta_keywords'     => 'Авточехлы',
                'url_alias'         => 'covers',
                'parent_id'         => 0,
                'related_attribute_id'  => null,
                'display_in_footer' => 1,
                'display_as_popular'    => 1,
                'sort_order'        => 1,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Коврики',
                'description'       => null,
                'image_id'          => 4,
                'meta_title'        => 'Коврики',
                'meta_description'  => 'Коврики',
                'meta_keywords'     => 'Коврики',
                'url_alias'         => 'mats',
                'parent_id'         => 0,
                'related_attribute_id'  => null,
                'display_in_footer' => 1,
                'display_as_popular'    => 1,
                'sort_order'        => 2,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Дефлекторы окон',
                'description'       => null,
                'image_id'          => 6,
                'meta_title'        => 'Дефлекторы окон',
                'meta_description'  => 'Дефлекторы окон',
                'meta_keywords'     => 'Дефлекторы окон',
                'url_alias'         => 'deflectors',
                'parent_id'         => 0,
                'related_attribute_id'  => null,
                'display_in_footer' => 1,
                'display_as_popular'    => 1,
                'sort_order'        => 3,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Дефлекторы капота',
                'description'       => null,
                'image_id'          => 5,
                'meta_title'        => 'Дефлекторы капота',
                'meta_description'  => 'Дефлекторы капота',
                'meta_keywords'     => 'Дефлекторы капота',
                'url_alias'         => 'deflectors2',
                'parent_id'         => 0,
                'related_attribute_id'  => null,
                'display_in_footer' => 1,
                'display_as_popular'    => 1,
                'sort_order'        => 4,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Автоаксессуары',
                'description'       => null,
                'image_id'          => 1,
                'meta_title'        => 'Автоаксессуары',
                'meta_description'  => 'Автоаксессуары',
                'meta_keywords'     => 'Автоаксессуары',
                'url_alias'         => 'accessories',
                'parent_id'         => 0,
                'related_attribute_id'  => null,
                'display_in_footer' => 0,
                'display_as_popular'    => 0,
                'sort_order'        => 5,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ]);
    }
}