<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key'   => 'meta_title',
                'value' => 'AutoSlon',
                'autoload'  => 1
            ],
            [
                'key'   => 'meta_description',
                'value' => 'Интернет-магазин автозапчастей AutoSlon',
                'autoload'  => 1
            ],
            [
                'key'   => 'meta_keywords',
                'value' => 'автозапчасти',
                'autoload'  => 1
            ],
            [
                'key'   => 'about',
                'value' => 'Текст "О нас"',
                'autoload'  => 0
            ],
            [
                'key'   => 'terms',
                'value' => 'Политика безопасности',
                'autoload'  => 0
            ],
            [
                'key'   => 'phones',
                'value' => json_encode(['050-417-96-65', '099-999-99-99', '067-000-00-00']),
                'autoload'  => 1
            ],
            [
                'key'   => 'notify_emails',
                'value' => json_encode(['admin@laravel.com', 'manager@laravel.com']),
                'autoload'  => 0
            ],
            [
                'key'   => 'socials',
                'value' => json_encode([
                    'facebook' => '',
                    'vkontakte' => '',
                    'instagram' => '',
                    'youtube'   => '',
                ]),
                'autoload'  => 0
            ]
        ]);
    }
}
