<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    /**
     *  Размеры изображений
     */
    'sizes' => [
        'product_list' => [
            'description' => 'Размер изображения товара в категории',
            'width' => 213,
            'height' => 180
        ],
        'product' => [
            'description' => 'Размер главного изображения в карточке товара',
            'width' => 445,
            'height' => 458
        ],
        'product_thumb' => [
            'description' => 'Изображения-ссылки для галереи товара',
            'width' => 98,
            'height' => 94
        ],
        'news_list' => [
            'description' => 'Размер изображения блога',
            'width' => 263,
            'height' => 165
        ],
        'article' => [
            'description' => 'Размер изображения блога',
            'width' => 730,
            'height' => 326
        ],
        'slide' => [
            'description' => 'Размер изображения слайда',
            'width' => 1170,
            'height' => 383
        ]
    ],

    /**
     *  Типы изображений
     */
    'types' => [
        'default' => [
            'description' => 'Тип по умолчанию',
        ],
        'product' => [
            'description' => 'Изображение для продукта',
            'sizes' => [
                'product',
                'product_list',
                'product_thumb'
            ]
        ],
        'news' => [
            'description' => 'Изображение для постов',
            'sizes' => [
                'news_list',
                'news'
            ]
        ],
        'slide' => [
            'description' => 'Изображение для слайда',
            'sizes' => [
                'slide'
            ]
        ]
    ]

);
