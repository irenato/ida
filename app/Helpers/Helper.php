<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 10.12.17
     * Time: 14:37
     */

    namespace App\Helpers;


    class Helper
    {
        public static function spinnex($string)
        {
            return $string = preg_replace_callback('~{.+?}~', function ($matches) {
                $result = explode('|', str_replace(['{', '}'], '', $matches[0]));
                return $result[rand(0, (count($result) - 1))];
            },
                $string);
        }
    }