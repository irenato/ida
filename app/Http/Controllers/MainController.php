<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\Module;
use App\Models\Product;
use App\Models\Article;
use App\Models\Image;
use Carbon\Carbon;
use App\Http\Requests;

class MainController extends Controller
{
    public function index(Product $products, Image $image, Setting $setting)
    {

        $modules_settings = Module::all();
        $latest_products = null;
        $bestseller_products = null;
        $slideshow = null;

        foreach ($modules_settings as $module_setting) {
            if ($module_setting->alias_name == 'latest') {
                $latest_settings = json_decode($module_setting->settings);
                $latest_status = $module_setting->status;
            } elseif ($module_setting->alias_name == 'bestsellers') {
                $bestseller_settings = json_decode($module_setting->settings);
                $bestseller_status = $module_setting->status;
            } elseif ($module_setting->alias_name == 'slideshow') {
                $slideshow_settings = json_decode($module_setting->settings);
                $slideshow_status = $module_setting->status;
            }
        }

        if($latest_status) {
            $latest_products = $products->getProducts($latest_settings->products, $latest_settings->quantity);

            if (!$latest_products->isEmpty()) {
                foreach ($latest_products as $product) {
                    $product->reviews = $product->reviews->where('published', 1);
                }
            }
        }

        if ($bestseller_status) {
            $bestseller_products = $products->getProducts($bestseller_settings->products, $bestseller_settings->quantity);

            if (!$bestseller_products->isEmpty()) {
                foreach ($bestseller_products as $product) {
                    $product->reviews = $product->reviews->where('published', 1);
                }
            }
        }

        if ($slideshow_status) {
            if (!empty($slideshow_settings->slides)) {
                uasort($slideshow_settings->slides, array($this, 'sortSlideshow'));
                $iterator = 0;

                foreach ($slideshow_settings->slides as $slide) {
                    if($slide->enable) {
                        $iterator++;
                        $slideshow[] = [
                            'image' => $image->get_file_url((int)$slide->image_id, 'slide'),
                            'link'  => $slide->link
                        ];
                    }
                    if ($iterator == $slideshow_settings->quantity) break;
                }
            }
        }

        $articles = Article::where('published', 1)->orderBy('updated_at', 'desc')->take(4)->get();

        setlocale(LC_TIME, 'RU');

        foreach ($articles as $key => $article) {
            $articles[$key]->date = iconv("cp1251", "UTF-8", $articles[$key]->updated_at->formatLocalized('%d.%m.%Y'));
        }

        $about = $setting->get_setting('about');
        return view('index', [
            'latest_products'       => $latest_products,
            'bestseller_products'   => $bestseller_products,
            'slideshow'             => $slideshow,
            'about'                 => $about,
            'news'                  => $articles
        ]);
    }

    public function sortSlideshow($slide1, $slide2)
    {
        if ($slide1->sort_order < $slide2->sort_order) return -1;
        elseif ($slide1->sort_order > $slide2->sort_order) return 1;
        else return 0;
    }
}
