<?php

namespace App\Http\Controllers\SeoFilter;

use Illuminate\Http\Request;
use App\Models\SeoFilter\SeoFilter;
use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;
// use Models\SeoFilter\SeoFilter;

class SeoFilterController extends Controller
{
    protected $rules = [
        'url' => 'required|max:255',
        'meta_title' => 'max:255',
        'meta_description' => 'max:255',
        'meta_keywords' => 'max:255',
        'meta_canonical' => 'max:255',
        'meta_robots' => 'max:255',
    ];
    protected $messages = [
        'url.required' => 'Поле должно быть заполнено!',
        '*.max' => 'Максимальная длина ключевых слов не может превышать 255 символов!',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.seo-filter.index', [
            'seo_filters' => SeoFilter::paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.seo-filter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SeoFilter $seo)
    {
        
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $seo->fill($request->except('_token'));
        $seo->content = htmlentities($request->content);
        $seo->save();

        return redirect('/admin/seo-filter')
            ->with('message-success', 'Страница SEO Фильтра успешно добавлена.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SeoFilter $seo)
    {
        return view('admin.seo-filter.edit')
            ->with('seo', $seo->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->rules;

        $validator = Validator::make($request->all(), $rules, $this->messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $content = SeoFilter::find($id);
        $content->fill($request->except('_token'));
        $content->content = htmlentities($request->content);
        $content->save();

        return redirect('/admin/seo-filter')
            ->with('message-success', 'Seo урл успешно обновлена.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = SeoFilter::find($id);
        $content->delete();

        return redirect('/admin/seo-filter')
            ->with('message-success', 'Seo фильр успешно удален.');
    }

    public function show($alias)
    {
        $content = Page::where('url_alias', $alias)->first();
        if (is_null($content))
            abort(404);

        return view('public.html_pages')
            ->with('content', $content);
    }
}
