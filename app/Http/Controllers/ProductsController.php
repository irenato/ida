<?php

    namespace App\Http\Controllers;

    use App\Attributes;
    use App\Models\Attribute;
    use App\Models\AttributeValue;
    use App\Models\Product;
    use Hamcrest\Core\Set;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Cookie;
    use phpDocumentor\Reflection\Types\Object;
    use Validator;

    use App\Http\Requests;
    use App\Models\Category;
    use App\Models\Setting;
    use App\Models\Module;
    use App\Models\Image;
    use Excel;
    use App\Models\Gallery;

    class ProductsController extends Controller
    {

        private $products;
        private $rules
            = [
                'name'                => 'required|unique:products',
                'price'               => 'required|numeric',
                'articul'             => 'required|unique:products',
                'quantity'            => 'numeric',
                'capacity'            => 'numeric',
                'product_category_id' => 'required|not_in:0',
                'meta_title'          => 'required|max:75',
                'meta_description'    => 'max:180',
                'meta_keywords'       => 'max:180',
                'url_alias'           => 'required|unique:products|unique:categories',
            ];

        private $messages
            = [
                'name.required'              => 'Поле должно быть заполнено!',
                'name.unique'                => 'Название товара должно быть уникальным!',
                'price.required'             => 'Поле должно быть заполнено!',
                'price.numeric'              => 'Значение должно быть числовым!',
                'articul.required'           => 'Поле должно быть заполнено!',
                'articul.unique'             => 'Артикул товара должен быть уникальным!',
                'quantity.numeric'           => 'Значение должно быть числовым!',
                'capacity.numeric'           => 'Значение должно быть числовым!',
                'product_category_id.not_in' => 'Не выбрана категория товара!',
                'meta_title.required'        => 'Поле должно быть заполнено!',
                'meta_title.max'             => 'Максимальная длина заголовка не может превышать 75 символов!',
                'meta_description.max'       => 'Максимальная длина описания не может превышать 180 символов!',
                'meta_keywords.max'          => 'Максимальная длина ключевых слов не может превышать 180 символов!',
                'url_alias.required'         => 'Поле должно быть заполнено!',
                'url_alias.unique'           => 'Значение должно быть уникальным для каждой категории!',
            ];

        public $sort
            = [
                'date_added'    => [
                    'name' => 'Дата добавления',
                    'dest' => 'DESC',
                    'sort' => 'created_at',
                ],
                'date_modified' => [
                    'name' => 'Дата изменения',
                    'dest' => 'DESC',
                    'sort' => 'updated_at',
                ],
                'name_asc'      => [
                    'name' => 'Имя (А-Я)',
                    'dest' => 'ASC',
                    'sort' => 'name',
                ],
                'name_desc'     => [
                    'name' => 'Имя (Я-А)',
                    'dest' => 'DESC',
                    'sort' => 'name',
                ],
            ];

        public $show = [15, 30, 45, 60];

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {

            $category_id  = false;
            $stock        = false;
            $current_sort = false;

            if ($request->sort) {
                $current_sort          = $this->sort[$request->sort];
                $current_sort['value'] = $request->sort;
            }

            if ($request->show) {
                if ($request->cookie('show_list') == null || $request->cookie('show_list') !== $request->show) {
                    Cookie::queue('show_list', $request->show);
                }
                $current_show = $request->show;
            } else {
                if ($request->cookie('show_list') == null) {
                    Cookie::queue('show_list', 15);
                    $current_show = 15;
                } else {
                    $current_show = $request->cookie('show_list');
                }
            }


            if (isset($request->category)) {
                $category_id = $request->category;
            }
            if (isset($request->stock)) {
                $stock = $request->stock;
            }

            if ($request->search) {
                $search         = $this->search(new Product, $request);
                $products       = $search->products;
                $current_search = $request->search;
            } else {
                $products       = Product::when($category_id, function ($query) use ($category_id) {
                    return $query->where('product_category_id', $category_id);
                })
                    ->when(($stock !== false), function ($query) use ($stock) {
                        return $query->where('stock', $stock);
                    })
                    ->when($current_sort, function ($query) use ($current_sort) {
                        return $query->orderBy($current_sort['sort'], $current_sort['dest']);
                    })
                    ->paginate($current_show);
                $current_search = false;
            }

            return view('admin.products.index', [
                'products'       => $products,
                'categories'     => Category::all(),
                'array_sort'     => $this->sort,
                'current_sort'   => $current_sort,
                'array_show'     => $this->show,
                'current_show'   => $current_show,
                'current_search' => $current_search,
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.products.create', [
                'categories' => Category::all(),
                'attributes' => Attribute::all(),
            ]);
        }

        /**
         * Создание товара
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, Image $image, Product $products, Gallery $gallery)
        {
            $attributes_error = $this->validate_attributes($request->product_attributes);

            $validator = Validator::make($request->all(), $this->rules, $this->messages);

            $image_href = $image->find($request->image_id)->href;
            $request->merge(['href' => $image_href]);

            if ($validator->fails() || $attributes_error) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                    ->withErrors($validator)
                    ->with('attributes_error', $attributes_error);
            }

            $gallery_id = $gallery->add_gallery($request->gallery);

            $request->merge(['gallery_id' => $gallery_id]);

            $data = ['products' => $request->all()];

            if (!empty($request->product_attributes)) {
                $data['product_attributes'] = [];
                foreach ($request->product_attributes as $attribute) {
                    $data['product_attributes'][] = [
                        'attribute_id'       => $attribute['id'],
                        'attribute_value_id' => $attribute['value'],
                    ];
                }
            }

            $products->insert_product($data);

            return redirect('/admin/products')
                ->with('message-success', 'Товар ' . $products->name . ' успешно добавлен.');

        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('admin.products.edit', [
                'product'    => Product::find($id),
                'categories' => Category::all(),
                'attributes' => Attribute::all(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id, Image $image)
        {
            $rules              = $this->rules;
            $rules['name']      = 'required|unique:products,name,' . $id;
            $rules['articul']   = 'required|unique:products,articul,' . $id;
            $rules['url_alias'] = 'required|unique:categories|unique:products,url_alias,' . $id;

            $attributes_error = $this->validate_attributes($request->product_attributes);

            $validator = Validator::make($request->all(), $rules, $this->messages);

            $image_href = $image->find($request->image_id)->href;

            $request->merge(['href' => $image_href]);

            if ($validator->fails() || $attributes_error) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                    ->withErrors($validator)
                    ->with('attributes_error', $attributes_error);
            }

            $product = Product::find($id);

            $product->fill($request->except(['_token', 'gallery', 'product_attributes', 'href']));

            $product->gallery->images = json_encode($request->gallery);

            $product->push();

            if (!empty($request->product_attributes)) {
                foreach ($request->product_attributes as $attribute) {
                    $product_attributes[] = [
                        'product_id'         => $product->id,
                        'attribute_id'       => $attribute['id'],
                        'attribute_value_id' => $attribute['value'],
                    ];
                }

                $product->attributes()->delete();
                $product->attributes()->createMany($product_attributes);
            }

            $route = 'admin/products';
            if ($request->page) {
                $route .= '?page=' . $request->page;
            }

            return redirect($route)
                ->with('message-success', 'Товар ' . $product->name . ' успешно отредактирован.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $product = Product::find($id);
            $product->delete();

            return redirect()->back()
                ->with('message-success', 'Товар ' . $product->name . ' успешно удален.');
        }

        /**
         * Получение списка всех атрибутов
         *
         * @param Attribute $attributes
         *
         * @return string|void
         */
        public function getAttributes(Attribute $attributes)
        {

            $attr     = $attributes->all();
            $response = [];

            if (!empty($attr)) {
                foreach ($attr as $attribute) {
                    $response[] = [
                        'attribute_id'   => $attribute->id,
                        'attribute_name' => $attribute->name,
                    ];
                }
            }

            return json_encode($response);

        }

        /**
         * Получение списка значений переданного атрибута
         *
         * @param Attribute $attributes
         * @param Request   $request
         *
         * @return string|void
         */
        public function getAttributeValues(Attribute $attributes, Request $request)
        {

            $attribute = $attributes->find((int)$request->attribute_id);
            $response  = [];

            if ($attribute !== null) {
                foreach ($attribute->values as $value) {
                    $response[] = [
                        'attribute_value_id' => $value->id,
                        'attribute_value'    => $value->name,
                    ];
                }
            }

            return json_encode($response);

        }

        /**
         * Живой поиск для модулей
         *
         * @param Request $request
         * @param Product $products
         *
         * @return string|void
         */
        public function livesearch(Request $request, Product $products)
        {
            $results = $products->where('name', 'like', '%' . $request->search . '%')->paginate(5);

            foreach ($results as $result) {

                if ($result) {
                    $json[] = [
                        'product_id' => $result->id,
                        'name'       => $result->name,
                    ];
                }
            }

            if (!empty($json)) {
                return json_encode($json);
            } else {
                return json_encode([['empty' => 'Ничего не найдено!']]);
            }
        }

        /**
         * @param $alias
         *
         * @return mixed
         */
        public function show($alias, Request $request, Product $products)
        {
            $product = $products->where('url_alias', $alias)->first();
            if (is_null($product)) {
                abort(404);
            }

            setlocale(LC_TIME, 'RU');
            $reviews = $product->reviews()
                ->where('published', 1)
                ->orderBy('created_at', 'desc')
                ->get();

            $product_reviews = [];
            foreach ($reviews as $review) {
                $review->date = iconv("cp1251", "UTF-8", $review->updated_at->formatLocalized('%d.%m.%Y'));
                if (!is_null($review->parent_review_id)) {
                    $product_reviews[$review->parent_review_id]['comments'][] = $review;
                } else {
                    $product_reviews[$review->id]['parent'] = $review;
                }
            }

            $related_category_products = Product::where('product_category_id', $product->product_related_category_id)->get();

            foreach ($related_category_products as $related_product) {
                $related_product->reviews = $related_product->reviews->where('published', 1);
            }

            $related_category = Category::where('id', $product->product_related_category_id)->first();

            $product_attributes = [];

            if (!$product->attributes->isEmpty()) {
                foreach ($product->attributes as $attribute) {
                    if (isset($product_attributes[$attribute->info->name])) {
                        $product_attributes[$attribute->info->name] .= ', ' . $attribute->value->name;
                    } else {
                        $product_attributes[$attribute->info->name] = $attribute->value->name;
                    }
                }
            }

            // Если у товара нет галереи возвращаем его изображение
            if (empty($product->gallery)) {
                $gallery = [$product->image];
            } else {
                $gallery = $product->gallery->objects();
            }

            $viewed = json_decode($request->cookie('viewed'), true);

            if (!is_array($viewed)) {
                $viewed = [];
            }

            if (!in_array($product->id, $viewed)) {
                if (count($viewed) > 7) {
                    array_splice($viewed, -7);
                }
                $viewed[] = $product->id;
            }

            if (!is_null($viewed)) {
                $products_viewed = $products->getProducts($viewed);
            }

            return response(view('public.product')
                ->with('product', $product)
                ->with('gallery', $gallery)
                ->with('reviews', $product_reviews)
                ->with('related_products', $related_category_products)
                ->with('related_category', $related_category)
                ->with('product_attributes', $product_attributes)
                ->with('viewed', $products_viewed))
                ->withCookie(cookie('viewed', json_encode($viewed)));
        }

        /**
         * Страница поиска
         *
         * @param Product $products
         * @param Request $request
         *
         * @return mixed
         */
        public function search(Product $products, Request $request)
        {
            $search_text = $request->search;

            $id = $request->get('page', 1);

            $data = $products->search($search_text, $id, 8);

            return view('public.search')->with('products', $data)
                ->with('search_text', $search_text);
        }

        /**
         * Валидация атрибутов товара на одинаковые значения
         *
         * @param $attributes
         *
         * @return bool|string
         */
        public function validate_attributes($attributes)
        {
            $attributes_error = false;

            if (!empty($attributes)) {
                foreach ($attributes as $product_attribute) {
                    $product_attribute_values[] = $product_attribute['value'];
                }

                foreach (array_count_values($product_attribute_values) as $count_value) {
                    if ($count_value > 1) {
                        $attributes_error = 'Значения атрибутов не могут быть одинаковы!';
                        break;
                    }
                }
            }

            return $attributes_error;
        }

        /**
         * Импорт товаров
         *
         * @param Request  $request
         * @param Products $products
         *
         * @return mixed
         */
        public function upload(Request $request, Product $products)
        {

            //$data = $request->input('products');
            $update        = $request->input('update');
            $parsed_data   = '';
            $errors        = [];
            $prepared_data = [];

            if ($request->hasFile('import_file')) {

                $path = $request->file('import_file')->getRealPath();
                $data = Excel::load($path, function ($reader) {

                })->get();

                if (!empty($data) && $data->count()) {

                    foreach ($data as $row) {
                        $row_data = ['tables' => []];
                        foreach ($row as $key => $val) {
                            $field = ['options' => $this->get_field_options($key)];
                            if (!isset($row_data['tables'][$field['options']['table']])) {
                                $row_data['tables'][$field['options']['table']] = [];
                            }

                            if (isset($field['options']['selector'])) {
                                $vals = explode($field['options']['selector'], $val);
                                foreach ($vals as $result) {
                                    if (isset($field['options']['replace'])) {
                                        $result = $this->replace_inserted_data($result, $field['options']['replace']['table'], $field['options']['replace']['find'], $field['options']['replace']['replaced']);
                                    }

                                    $new_row = [$field['options']['field'] => $result];
                                    if (isset($field['options']['attached_fields'])) {
                                        $new_row = array_merge($new_row, $field['options']['attached_fields']);
                                    }
                                    $row_data['tables'][$field['options']['table']][] = $new_row;
                                }
                            } else {
                                if (isset($field['options']['replace']) && !empty($val)) {
                                    $val = $this->replace_inserted_data($val, $field['options']['replace']['table'], $field['options']['replace']['find'], $field['options']['replace']['replaced']);
                                }

                                if (isset($field['options']['unique'])) {
                                    $row_data['tables'][$field['options']['table']][$field['options']['field']] = $val;
                                } else {
                                    $row_data['tables'][$field['options']['table']][] = [$field['options']['field'] => $val];
                                }
                            }
                        }
                        $prepared_data[] = $row_data;
                    }

                    foreach ($prepared_data as $product) {
                        if ($product['tables']['products']['name']) {
                            if ($update) {
                                $products->update_product($product['tables']);
                            } else {
                                $products->insert_product($product['tables']);
                            }
                        }
                        if (isset($product['errors'])) {
                            $errors[] = $product;
                        }
                    }
                }

            }


//        if(!empty($data)){
//            $parsed_data = $this->products_parser($data);
//            foreach($parsed_data as $product) {
//                if($update)
//                    $products->update_product($product);
//                else
//                    $products->insert_product($product);
//
//                if(isset($product['errors']))
//                    $errors[] = $product;
//            }
//        }


            return view('admin.products.upload')
                ->with('data', $prepared_data)
                ->with('errors', $errors);
        }

        /**
         * Парсинг опций вставки
         *
         * @param $field
         *
         * @return array|bool
         */
        public function get_field_options($field)
        {
            $params = explode('.', $field);
            if (count($params) < 2) {
                return false;
            }
            $options = [
                'table' => $params[0],
                'field' => $params[1],
            ];
            $count   = count($params);
            if ($count > 2) {
                for ($i = 2; $i < $count; $i++) {
                    if (strpos($params[$i], 'selector') === 0) {
                        $options['selector'] = preg_replace('/selector\((.+)\)/', '$1', $params[$i]);
                    } elseif (strpos($params[$i], 'attached_field') === 0) {
                        if (!isset($options['attached_fields'])) {
                            $options['attached_fields'] = [];
                        }
                        $attached_field                                 = explode(':', preg_replace('/attached_field\((.+)\)/', '$1', $params[$i]), 2);
                        $options['attached_fields'][$attached_field[0]] = $attached_field[1];
                    } elseif ($params[$i] == 'unique') {
                        $options['unique'] = true;
                    } elseif (strpos($params[$i], 'replace') === 0) {
                        if (!isset($options['attached_fields'])) {
                            $options['attached_fields'] = [];
                        }
                        $replace            = explode(':', preg_replace('/replace\((.+)\)/', '$1', $params[$i]), 3);
                        $options['replace'] = ['table' => $replace[0], 'find' => $replace[1], 'replaced' => $replace[2]];
                    }
                }
            }
            return $options;
        }

        /**
         * Получение одного поля таблицы по другому
         *
         * @param $data
         * @param $table
         * @param $find
         * @param $replaced
         *
         * @return mixed
         */
        public function replace_inserted_data($data, $table, $find, $replaced)
        {
            if ($table == 'images' && filter_var($data, FILTER_VALIDATE_URL)) {
                $filename = time() . basename($data);
                \Intervention\Image\Facades\Image::make($data)->save(public_path('assets/images/' . $filename));
                $data  = $filename;
                $image = new Image();
                if (!$image->get_id_by_title($data)) {
                    $image->href  = $data;
                    $image->title = $data;
                    $image->sizes = '[]';
                    $image->save();
                }
            }
            $model_name = 'App\Models\\' . str_replace(' ', '', ucwords(str_replace('_', ' ', preg_replace('/s$/', '', $table))));
            if (!class_exists($model_name)) {
                $model_name = 'App\Models\\' . str_replace(' ', '', ucwords(str_replace('_', ' ', $table)));
            }
            if (!class_exists($model_name)) {
                return null;
            }

            $table  = new $model_name;
            $result = $table->select($replaced)->where($find, 'LIKE', '%' . $data . '%')->take(1)->get()->first();

            return $result !== null ? $result->$replaced : $result;
        }

        /**
         * Парсер csv товаров
         *
         * @param $data
         *
         * @return array
         */
        private function products_parser($data)
        {
            $image            = new Image;
            $attribute_values = AttributeValue::all();
            $rows             = explode(PHP_EOL, $data);
            $products         = [];
            $keys             = [
                'articul',
                'name',
                'product_description',
                'price',
                'capacity',
                'measure_id',
                'group',
                'character',
                'species',
                'material',
                'upper_note',
                'heart_note',
                'base_note',
                'diameter',
                'quantity',
                'color',
                'size',
                'stock',
                'brand',
                'product_category_id',
                'product_related_category_id',
                'sex',
                'href',
            ];

            $attributes_ids = [
                'sex'                         => [
                    'id'     => 3,
                    'values' => [
                        'женский' => 7,
                        'мужской' => 8,
                    ],
                ],
                'measure_id'                  => [
                    'values' => [
                        'мл' => 1,
                        'л'  => 2,
                        'шт' => 3,
                    ],
                ],
                'brand'                       => [
                    'id'     => 1,
                    'values' => [],
                ],
                'product_category_id'         => [
                    'values' => [
                        'парфюмерия'       => 1,
                        'флаконы'          => 2,
                        'аксессуары'       => 3,
                        'другая продукция' => 4,
                    ],
                ],
                'product_related_category_id' => [
                    'values' => [
                        'парфюмерия'       => 1,
                        'флаконы'          => 2,
                        'аксессуары'       => 3,
                        'другая продукция' => 4,
                    ],
                ],
                'species'                     => [
                    'id'     => 2,
                    'values' => [],
                ],
                'group'                       => [
                    'id'     => 4,
                    'values' => [],
                ],
                'color'                       => [
                    'id'     => 6,
                    'values' => [],
                ],
            ];

            foreach ($attributes_ids as $key => $val) {
                if (isset($val['id'])) {
                    foreach ($attribute_values as $value) {
                        if ($value->attribute_id == $val['id']) {
                            $attributes_ids[$key]['values'][mb_strtolower($value->name)] = $value->id;
                        }
                    }
                }
            }

            foreach ($rows as $row) {
                $row = explode('	', $row);
                if (count($keys) == count($row)) {
                    array_walk($row, [$this, 'trim_value']);
                    $product                       = array_combine($keys, $row);
                    $product['capacity']           = str_replace(['мл', 'л', ' '], '', $product['capacity']);
                    $product['group']              = explode(',', preg_replace('/(^"|"$|;$|\.$|,$)/', '', $product['group']));
                    $product['product_attributes'] = [];

                    if (empty($product['brand']) || empty($product['articul'])) {
                        $product['url_alias'] = mb_strtolower($this->rus2lat($product['name']) . '_' . $product['capacity']);
                    } else {
                        $product['url_alias'] = mb_strtolower($product['brand'] . '_' . $product['articul']);
                    }

                    $product['image_id'] = $image->get_id_by_title($product['href']);

                    if (empty($product['image_id'])) {
                        $product['image_id'] = 1;
                        if (!isset($product['errors'])) {
                            $product['errors'] = [];
                        }
                        $product['errors'][] = 'Не найдено изображение "' . $product['href'] . '"';
                    }

                    if ($product['stock'] == '') {
                        $product['stock'] = 1;
                    }

                    foreach ($product as $key => $val) {
                        if (is_string($val)) {
                            $product[$key] = preg_replace('/(^"|"$|;$|\.$|,$|,\s?,)/', '', preg_replace('@^\s*|\s*$@u', '', $val));
                        }

                        if (isset($attributes_ids[$key])) {
                            if (isset($attributes_ids[$key]['id']) && is_array($val)) {
                                foreach ($val as $attr) {
                                    $attr = preg_replace('@^\s*|\s*$@u', '', $attr);
                                    if (isset($attributes_ids[$key]['values'][mb_strtolower($attr)])) {
                                        $product['product_attributes'][] = [
                                            'attribute_id'       => $attributes_ids[$key]['id'],
                                            'attribute_value_id' => $attributes_ids[$key]['values'][mb_strtolower($attr)],
                                        ];
                                    } elseif (!empty($attr)) {
                                        if (!isset($product['errors'])) {
                                            $product['errors'] = [];
                                        }
                                        $product['errors'][] = 'Не распознан атрибут "' . $attr . '"';
                                    }
                                }
                                unset($product[$key]);
                            } else {
                                $val = preg_replace('@^\s*|\s*$@u', '', $val);
                                if (isset($attributes_ids[$key]['id'])) {
                                    if (isset($attributes_ids[$key]['values'][mb_strtolower($val)])) {
                                        $product['product_attributes'][] = [
                                            'attribute_id'       => $attributes_ids[$key]['id'],
                                            'attribute_value_id' => $attributes_ids[$key]['values'][mb_strtolower($val)],
                                        ];
                                    } elseif (!empty($val)) {
                                        if (!isset($product['errors'])) {
                                            $product['errors'] = [];
                                        }
                                        $product['errors'][] = 'Не распознан атрибут "' . $val . '"';
                                    }
                                    unset($product[$key]);
                                } elseif (isset($attributes_ids[$key]['values']) && isset($attributes_ids[$key]['values'][mb_strtolower($val)])) {
                                    $product[$key] = $attributes_ids[$key]['values'][mb_strtolower($val)];
                                } elseif (!empty($val)) {
                                    if (!isset($product['errors'])) {
                                        $product['errors'] = [];
                                    }
                                    $product['errors'][] = 'Не распознан атрибут "' . $val . '"';
                                }
                            }
                        }
                    }

                    $product['meta_title']       = $product['name'];
                    $product['meta_description'] = $product['product_description'];
                    $product['meta_keywords']    = $product['upper_note'] . ', ' . $product['heart_note'] . ', ' . $product['base_note'];

                    $products[] = $product;
                }
            }
            return $products;
        }

        /**
         * Удаление нежелательных символов
         *
         * @param $value
         */
        function trim_value(&$value)
        {
            if (is_string($value)) {
                $value = preg_replace('/(^"|"$|;$|\.$|,$|,\s?,)/', '', preg_replace('@^\s*|\s*$@u', '', $value));
            }
        }

        /**
         * Транслит
         *
         * @param $string
         *
         * @return mixed
         */
        public function rus2lat($string)
        {
            $converter = [
                'а' => 'a', 'б' => 'b', 'в' => 'v',
                'г' => 'g', 'д' => 'd', 'е' => 'e',
                'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
                'и' => 'i', 'й' => 'y', 'к' => 'k',
                'л' => 'l', 'м' => 'm', 'н' => 'n',
                'о' => 'o', 'п' => 'p', 'р' => 'r',
                'с' => 's', 'т' => 't', 'у' => 'u',
                'ф' => 'f', 'х' => 'h', 'ц' => 'c',
                'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
                'ь' => "", 'ы' => 'y', 'ъ' => "",
                'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

                'А' => 'A', 'Б' => 'B', 'В' => 'V',
                'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
                'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
                'И' => 'I', 'Й' => 'Y', 'К' => 'K',
                'Л' => 'L', 'М' => 'M', 'Н' => 'N',
                'О' => 'O', 'П' => 'P', 'Р' => 'R',
                'С' => 'S', 'Т' => 'T', 'У' => 'U',
                'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
                'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
                'Ь' => "", 'Ы' => 'Y', 'Ъ' => "",
                'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
            ];
            return strtr($string, $converter);
        }
    }
