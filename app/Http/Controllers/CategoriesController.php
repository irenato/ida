<?php

    namespace App\Http\Controllers;

    use App\Helpers\Helper;
    use App\Models\Attribute;
    use App\Models\Module;
    use App\Models\ProductAttribute;
    use Illuminate\Http\Request;
    use App\Models\Category;
    use App\Models\Image;
    use App\Models\Product;
    use App\Http\Requests;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Illuminate\Support\Facades\Session;
    use Validator;


    class CategoriesController extends Controller
    {

        private $rules
            = [
                'name'          => 'required',
                'meta_title'    => 'max:75',
                'meta_keywords' => 'max:180',
                'url_alias'     => 'required|unique:categories',
            ];

        private $messages
            = [
                'name.required'       => 'Поле должно быть заполнено!',
                'meta_title.required' => 'Поле должно быть заполнено!',
                'meta_title.max'      => 'Максимальная длина заголовка не может превышать 75 символов!',
                'meta_keywords.max'   => 'Максимальная длина ключевых слов не может превышать 180 символов!',
                'url_alias.required'  => 'Поле должно быть заполнено!',
                'url_alias.unique'    => 'Значение должно быть уникальным для каждой категории!',
            ];

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('admin.categories.index', ['categories' => Category::paginate(10)]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.categories.create', [
                'categories' => Category::all(),
                'attributes' => Attribute::all(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, Category $category)
        {

            $validator = Validator::make($request->all(), $this->rules, $this->messages);

            $image_id = $request->image_id ? $request->image_id : $article->image_id;
            $href     = Image::find($image_id)->href;

            $request->merge(['href' => $href]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                    ->withErrors($validator);
            }

            $category->fill($request->except('_token'));
            $category->description = !empty($request->description) ? Helper::spinnex($request->description) : null;
            $category->meta_title  = !empty($request->meta_title) ? $request->meta_title : (
                ($request->parent_id ? Category::find($request->parent_id)->name . ' - ' : '') . $request->name);
            $category->sort_order  = !empty($request->sort_order) ? $request->sort_order : 0;
            $category->url_alias   = strtolower(str_replace([' ', '-', '(', ')'], ['_', '_', '', ''], $category->url_alias));
            $category->meta_description = !empty($request->meta_description) ? Helper::spinnex($request->meta_description) : null;
            $category->save();

            return redirect('/admin/categories')
                ->with('message-success', 'Категория ' . $category->name . ' успешно добавлена.');
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('admin.categories.edit', [
                'attributes' => Attribute::all(),
                'category'   => Category::find($id),
                'categories' => Category::all(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {

            $rules              = $this->rules;
            $rules['url_alias'] = 'required|unique:categories,url_alias,' . $id;

            $validator = Validator::make($request->all(), $rules, $this->messages);

            $image_id = $request->image_id ? $request->image_id : $article->image_id;
            $href     = Image::find($image_id)->href;

            $request->merge(['href' => $href]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                    ->withErrors($validator);
            }

            $category = Category::find($id);
            $category->fill($request->except('_token'));
            $category->description = !empty($request->description) ? Helper::spinnex($request->description) : null;
            $category->meta_title  = !empty($request->meta_title) ? $request->meta_title : (
                ($request->parent_id ? Category::find($request->parent_id)->name . ' - ' : '') . $request->name);
            $category->sort_order  = !empty($request->sort_order) ? $request->sort_order : 0;
            $category->meta_description = !empty($request->meta_description) ? Helper::spinnex($request->meta_description) : null;
            $category->save();

            return redirect('/admin/categories')
                ->with('message-success', 'Категория ' . $category->name . ' успешно обновлена.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $category = Category::find($id);
            $category->delete();

            return redirect('/admin/categories')
                ->with('message-success', 'Категория ' . $category->name . ' успешно удалена.');
        }

        public function show($alias, Request $request, Category $categories, Product $product)
        {
            $per_page = config('view.product_quantity');

            $sort_array = [
                [
                    'value' => 'popularity',
                    'name'  => 'популярности',
                ],
                [
                    'value' => 'date',
                    'name'  => 'дате',
                ],
                [
                    'value' => 'name',
                    'name'  => 'названию',
                ],
            ];

            $filter = [];

            if ($request->filters !== null) {
                foreach ($request->filters as $filters) {
                    $attribute                             = explode('_', $filters);
                    $filter['attributes'][$attribute[0]][] = $attribute[1];
                }
            }

            if ($request->prices !== null) {
                $filter['price'] = $request->prices;
            }

            $current_sort = $request->sort ? $request->sort : 'popularity';

            if ($alias !== 'popular' && $alias !== 'new') {
                $category = $categories->where('url_alias', $alias)->first();
                if (is_null($category)) {
                    abort(404);
                }

                $category_id = $category->id;

            } elseif ($alias == 'popular') {

                $category_id = 'bestsellers';
                $category    = [
                    'meta_title' => 'Популярные товары',
                    'name'       => 'Популярные',
                    'url_alias'  => 'popular',
                ];

                $new_settings = json_decode(Module::where('alias_name', 'bestsellers')->value('settings'));
                $take         = $new_settings->quantity;

            } elseif ($alias == 'new') {

                $category_id = 'new';
                $category    = [
                    'meta_title' => 'Новые товары',
                    'name'       => 'Новинки',
                    'url_alias'  => 'new',
                ];

                $new_settings = json_decode(Module::where('alias_name', 'latest')->value('settings'));
                $take         = $new_settings->quantity;
            }
//dd($current_sort);
            $products_info = $product->getProductsByCategory($category_id, $filter, $current_sort);

            if (!empty($products_info['prices'])) {
                $prices = [
                    'min' => floor(min($products_info['prices'])),
                    'max' => ceil(max($products_info['prices'])),
                ];
            }


            if ($request->json !== null) {
                return response()->json($products_info['products']->count());
            }

            $paginator_options = [
                'path'  => '/categories/' . $alias,
                'query' => [
                    'sort' => $current_sort,
                ],
            ];

            if (!empty($filter['price'])) {
                $paginator_options['query']['prices'] = [
                    'min' => $filter['price']['min'],
                    'max' => $filter['price']['max'],
                ];
            }

            if (!empty($filter['attributes'])) {
                foreach ($filter['attributes'] as $attribute_id => $attribute_value) {
                    foreach ($attribute_value as $attribute_value_id) {
                        $paginator_options['query']['filters'][] = $attribute_id . '_' . $attribute_value_id;
                    }
                }
            }

            $current_page          = LengthAwarePaginator::resolveCurrentPage();
            $current_page_products = $products_info['products']->slice(($current_page - 1) * $per_page, $per_page)->all();
            $products              = new LengthAwarePaginator($current_page_products, count($products_info['products']), $per_page, $current_page, $paginator_options);

            $viewed = json_decode($request->cookie('viewed'), true);

            if (!is_null($viewed)) {
                $viewed = $product->getProducts($viewed);
            }

            return view('public.category')
                ->with('category', $category)
                ->with('products', $products)
                ->with('prices', isset($prices) ? $prices : null)
                ->with('attributes', $products_info['attributes'])
                ->with('sort_array', $sort_array)
                ->with('current_sort', $current_sort)
                ->with('filters', $filter)
                ->with('viewed', $viewed);
        }
    }
