<?php

namespace App\Providers;

use App\Categories;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Models\Page;
use App\Models\Setting;
use App\Models\Category;
use App\Models\Wishlist;
use App\Models\Cart;
use App\Models\User;
use App\Models\Review;
use App\Models\Order;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Setting $setting, Cart $cart)
    {

        $user = Sentinel::getUser();
        if(!is_null($user)) {
            $user = User::find($user->id);
        }

        $settings = $setting->get_global();

        if(!is_null($user)) {

            $orders = Order::where('status_id', 1)->count();
            $reviews = Review::where('new', 1)->count();

            view()->composer('admin.layouts.main', function($view) use ($user, $orders, $reviews) {
                $view->with([
                    'user'          => $user,
                    'new_orders'    => $orders,
                    'new_reviews'   => $reviews
                ]);
            });
        }

        view()->composer([
            'public/*', 'users/*', 'index', 'login', 'register'
        ], function($view) use ($settings, $user, $cart) {

            $pages = Page::where('status', 1)->get();
            $categories = Category::where(['status'=> 1, 'parent_id' => 0])->get();
            $popular_categories = array_values($categories->where('display_as_popular', 1)->all());

            $cart = $cart->current_cart();

            $view->with([
                'settings' => $settings,
                'user' => $user ? $user : false,
                'html_pages' => $pages,
                'categories' => $categories,
                'popular_categories' => $popular_categories,
                'cart' => $cart
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
