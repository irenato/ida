<?php

namespace App\Models\SeoFilter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeoFilter extends Model
{
	protected $fillable = ['id', 'seo_title', 'seo_key', 'seo_desc', 'content', 'seo_robots', 'seo_canonical', 'url'];
}
