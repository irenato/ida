<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'articles';

    public $fillable = [
        'user_id',
        'url_alias',
        'title',
        'text',
        'published',
        'image_id',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }
}
