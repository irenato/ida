<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;

class Product extends Model
{

    use SoftDeletes;

    protected $fillable =[
        'name',
        'description',
        'short_description',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'url_alias',
        'articul',
        'price',
        'product_category_id',
        'product_related_category_id',
        'image_id',
        'gallery_id',
        'quantity',
        'stock',
        'rating'
    ];

    protected $table = 'products';
    protected $dates = ['deleted_at'];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'product_categories', 'product_id', 'category_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'product_category_id');
    }

    public function image()
    {
        return $this->hasOne('App\Models\Image', 'id', 'image_id');
    }

    public function attributes()
    {
        return $this->hasMany('App\Models\ProductAttribute', 'product_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review', 'product_id');
    }

    public function wishlist()
    {
        return $this->hasMany('App\Models\Wishlist', 'product_id');
    }

    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery');
    }

    /**
     * Поиск товаров
     * @param string $text Поисковый текст
     * @param int $page Номер страницы
     * @param int $count Колличество на странице
     * @return LengthAwarePaginator
     */
    public function search($text = '', $page = 1, $count = 8)
    {
        $all_products = $this->where('name', 'like', '%'.$text.'%');

        $paginate = 1;

        $all_count = $this->where('name', 'like', '%'.$text.'%')->count();

        $data = new LengthAwarePaginator($all_products->skip($count*($page-1))->take($count)->get(), ceil($all_count/$count), $paginate, $page, array('path' => 'search/'));

        return $data;
    }

    public function get_attributes()
    {
        return $this->hasMany('App\Models\ProductAttribute', 'product_id');
    }

    /**
     * Создание продукта
     * @param $data
     * @return string
     */
    public function insert_product($data)
    {
        if(!isset($data['products']['url_alias'])){
            $data['products']['url_alias'] = $this->generate_alias($data['products']);
        }

        if($this->isset_product_with_url_alias($data['products']['url_alias']) == false) {
            $result_data = $this->get_prepared_data($data);

            $id = $this->insertGetId($result_data['products']);
            $product = $this->find($id);
            if(!empty($result_data['product_attributes']))
                $product->attributes()->createMany($result_data['product_attributes']);

            if(!empty($result_data['categories']))
                $product->categories()->attach($result_data['categories']);

            return $id;
        } else {
            return 'already_exist';
        }
    }

    /**
     * Обновление продукта
     * @param $data
     * @return string
     */
    public function update_product($data)
    {
        if(!isset($data['products']['url_alias'])){
            $data['products']['url_alias'] = $this->generate_alias($data['products']);
        }

        if($this->isset_product_with_url_alias($data['products']['url_alias']) == false) {
            $id = $this->insert_product($data);
        }else{
            $result_data = $this->get_prepared_data($data);

            $product = $this->select('id')
                ->where('url_alias', $data['products']['url_alias'])
                ->take(1)
                ->get()
                ->first();

            $id = $product->id;

            $this->where('id', $id)->update($result_data['products']);

            $product->attributes()->where('product_id', $id)->delete();
            if (!empty($result_data['product_attributes']))
                $product->attributes()->createMany($result_data['product_attributes']);

            if(!empty($result_data['categories']))
                $product->categories()->attach($result_data['categories']);
        }
        return $id;
    }

    /**
     * Подготовка данных к вставке
     * @param $data
     * @return array
     */
    public function get_prepared_data($data)
    {
        $fills = [
            'products' => $this->fillable,
            'product_attributes' => [
                'attribute_id',
                'attribute_value_id'
            ],
            'product_categories' => [
                'category_id'
            ]
        ];

        foreach (['rating'] as $key) {
            while (($i = array_search($key, $fills['products'])) !== false) {
                unset($fills['products'][$i]);
            }
        }

        $result_data = [];
        foreach ($fills as $table => $table_fills) {
            if(!isset($result_data[$table]))
                $result_data[$table] = [];

            if($table == 'product_categories' && isset($data['product_categories'])){
                if (!isset($data[$table]) && isset($data['categories']))
                    $result_data['categories'] = $data['categories'];
                else {
                    foreach ($data[$table] as $category) {
                        $result_data['categories'][] = $category['category_id'];
                    }
                }
            }elseif ($table != 'product_attributes') {
                foreach ($table_fills as $fill) {
                    $result_data[$table][$fill] = isset($data[$table][$fill]) ? $data[$table][$fill] : ($fill == 'stock' ? 1 : null);
                }
            }else{
                if(isset($data['product_attributes'] )) {
                    foreach ($data['product_attributes'] as $attribute) {
                        if(!empty($attribute['attribute_value_id']))
                            $result_data[$table][] = $attribute;
                    }
                }
            }
        }

        if(empty($result_data['products']['meta_title']))
            $result_data['products']['meta_title'] = $result_data['products']['name'];

        if(empty($result_data['products']['short_description']))
            $result_data['products']['short_description'] = $result_data['products']['name'];

        if(empty($result_data['products']['meta_description']))
            $result_data['products']['meta_description'] = $result_data['products']['short_description'];

        if(empty($result_data['products']['meta_keywords']))
            $result_data['products']['meta_keywords'] = $result_data['products']['name'];

        if(!isset($result_data['categories']) && !empty($result_data['products']['product_category_id']))
            $result_data['categories'] = [$result_data['products']['product_category_id']];

        return $result_data;
    }

    /**
     * Проверка существования продукта с таким url_alias
     * @param $url_alias
     * @return bool
     */
    public function isset_product_with_url_alias($url_alias)
    {
        return (bool)$this->where('url_alias', $url_alias)
            ->take(1)
            ->count();
    }

    /**
     * Генерация алиаса
     * @param $product
     * @return ЧПУ
     */
    public function generate_alias($product){
        if(empty($product['brand']) || empty($product['articul']))
            $url_alias = mb_strtolower($this->rus2lat($product['name']));
        else
            $url_alias = mb_strtolower($product['brand'] . '_' . $product['articul']);

        $url_alias = str_replace(array(' ', '-', '(', ')', '.', '→', '/', ','), array('_', '_', '', '', '', '', '', '_'), $url_alias);

        return $url_alias;
    }

    /**
     * Транслит
     * @param $string
     * @return mixed
     */
    public function rus2lat($string)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => "",    'ы' => 'y',   'ъ' => "",
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => "",    'Ы' => 'Y',   'Ъ' => "",
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    public function getProducts($data, $take = false)
    {
        if (is_array($data)){
            return $this->whereIn('id', $data)
                ->when($take, function ($query) use ($take) {
                    return $query->take($take);
                })
                ->get();
        }
    }

    public function getProductsByCategory($category_id, $filter, $sort)
    {

        if ($sort == 'popularity') {
            $orderBy = 'rating';
            $route = 'desc';
        } elseif ($sort == 'date') {
            $orderBy = 'updated_at';
            $route = 'desc';
        } elseif ($sort == 'name') {
            $orderBy = 'name';
            $route = 'asc';
        }

        $products = Product::select('products.*');

        if ($category_id == 'bestsellers') {
            $products->join('module_bestsellers', 'products.id', '=', 'module_bestsellers.product_id');
        } elseif ($category_id == 'new') {
            $products->join('module_new_products', 'products.id', '=', 'module_new_products.product_id');
        }elseif ($category_id) {
            $category = Category::find($category_id);
            $children_categories = array_merge([$category->id], $category->children_ids());

            $products->join('product_categories AS cat', 'products.id', '=', 'cat.product_id')->whereIn('cat.category_id', $children_categories);
        }

        $products_info = $products->get();

        $prices = [];
        $attributes = [];
        foreach ($products_info as $product_info) {
            $prices[] = $product_info->price;
            foreach ($product_info->get_attributes as $attribute) {
                $attributes[$attribute->attribute_id] = $attribute->info;
            }

        }

        $prices = array_unique($prices);

        if (!empty($filter['attributes'])) {

            foreach ($filter['attributes'] as $key => $attribute) {

                $products->join('product_attributes AS attr' . $key, 'products.id', '=', 'attr' . $key . '.product_id');
                $products->where('stock', 1);
                $products->where('attr' . $key . '.attribute_id', $key);
                $products->where(function($query) use($attribute, $key){

                    foreach ($attribute as $attribute_value) {
                        $query->orWhere('attr' . $key . '.attribute_value_id', $attribute_value);
                    }
                });

            }
        }

        if (!empty($filter['price'])) {
            $products->whereBetween('price', $filter['price']);
        }

        $products = $products->orderBy($orderBy, $route)->groupBy('products.id')->get();

        $result = [
            'prices'        => $prices,
            'attributes'    => $attributes,
            'products'      => $products
        ];

        return $result;

    }

}