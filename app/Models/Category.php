<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'image_id',
        'display_as_popular',
        'display_in_footer',
        'url_alias',
        'related_attribute_id',
        'parent_id',
        'sort_order',
        'status'
    ];

    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'product_category_id', 'id');
    }

    public function related_attributes()
    {
        return $this->hasMany('App\Models\AttributeValue', 'attribute_id', 'related_attribute_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    public function children() {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->with('children');
    }

    /**
     * ID всех вложенных категорий
     *
     * @return array
     */
    public function children_ids(){
        return $this->parse_children_ids($this->children()->get());
    }

    public function parse_children_ids($categories){
        $ids = [];
        foreach ($categories as $category){
            $ids[] = $category->id;
            if(!$category->children->isEmpty()){
                $ids = array_merge($ids, $this->parse_children_ids($category->children));
            }
        }

        return $ids;
    }
}
