<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = [
        'user_id',
        'user_ip',
        'session_id',
        'products',
        'total_quantity',
        'total_price',
        'user_data',
        'cart_data'
    ];

    /**
     * Получение корзины пользователя
     * @param $create - Создавать новую корзину или нет
     */
    public function current_cart($create = false){

        $user = Sentinel::check();

        if($user) {
            $user_id = $user->id;
            $cart = $this->where('user_id', $user_id)->first();

            if(!is_null($cart) && $cart->session_id != Session::getId())
                $cart->update(['session_id' => Session::getId()]);
        } else {
            $user_id = 0;
            $cart = $this->where('session_id', Session::getId())->first();
        }

        if(is_null($cart) && $create) {
            $cart = $this->create_cart($user_id);
        }

        return $cart;
    }

    /**
     * Создание новой корзины
     *
     * @param $user_id
     */
    public function create_cart($user_id){

        $id = $this->insertGetId([
            'user_id' => $user_id,
            'session_id' => Session::getId(),
            'products' => null,
            'total_quantity' => 0,
            'total_price' => 0
        ]);

        return $this->where('id', $id)->first();
    }

    /**
     * Обновление корзины
     */
    public function update_cart(){
        $products = json_decode($this->products, true);

        $total_quantity = 0;
        $total_price = 0;

        foreach ($products as $product_id => $product_quantity) {
            $total_quantity += $product_quantity;
            $total_price += Product::find($product_id)->price * (int)$product_quantity;
        }

        $this->update(['total_quantity' => $total_quantity, 'total_price' => $total_price]);

    }

    /**
     * Получение продуктов из корзины
     *
     * @return array
     */
    public function get_products()
    {
        $current_cart = $this->current_cart();
        $products_in_cart = json_decode($current_cart->products, true);
        $products = [];

        if(!is_null($products_in_cart)) {
            foreach ($products_in_cart as $product_id => $product_quantity) {
                $products[] = [
                    'product'   => Product::find($product_id),
                    'quantity'  => $product_quantity
                ];
            }
        }

        return $products;
    }

    /**
     * Добавление товара в корзину
     *
     * @param $product_id
     * @param int $quantity
     */
    public function add_product($product_id, $quantity = 1)
    {
        $products = json_decode($this->products, true);
        if(is_null($products))
            $products = [];

        if(array_key_exists($product_id, $products)){
            $products[$product_id] = $products[$product_id] + $quantity;
        } else {
            $products[$product_id] = $quantity;
        }

        $this->update(['products' => json_encode($products)]);
        $this->update_cart();
    }

    /**
     * Удаление товара из корзины
     *
     * @param $product_id
     */
    public function remove_product($product_id)
    {
        $products = json_decode($this->products, true);
        unset($products[$product_id]);

        $this->update(['products' => json_encode($products)]);
        $this->update_cart();
    }

    /**
     * Изменение колличества товара в корзине на указанную величину
     *
     * @param $product_id
     * @param $delta
     */
    public function increment_product_quantity($product_id, $delta)
    {
        if ($this->product_isset($product_id)) {

            $products = json_decode($this->products, true);
            $products[$product_id] = $products[$product_id] + $delta;
            $this->products = json_encode($products);
            $this->update_cart();

        } elseif ($delta > 0) {
            $this->add_product($product_id, $delta);
        }
    }

    /**
     * Изменение колличества товара в корзине
     *
     * @param $product_id
     * @param $quantity
     */
    public function update_product_quantity($product_id, $quantity)
    {
        if ($quantity <= 0) {
            $this->remove_product($product_id);
        }

        if ($this->product_isset($product_id)) {
            $products = json_decode($this->products, true);
            $products[$product_id] = $quantity;
            $this->update(['products' => json_encode($products)]);
            $this->update_cart();
        } else {
            $this->add_product($product_id, $quantity);
        }
    }

    /**
     * Проверка наличия товара в корзине
     *
     * @param $product_id
     * @return bool
     */
    public function product_isset($product_id)
    {
        $products = json_decode($this->products, true);

        if(is_null($products)) {
            return false;
        } else {
            return array_key_exists($product_id, $products);
        }
    }
}