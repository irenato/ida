<?php

namespace App\Models;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\DB;

class User extends \Cartalyst\Sentinel\Users\EloquentUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
    ];
    /**
     * Array of login column names.
     *
     * @var array
     */
    protected $loginNames = ['phone'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blog()
    {
        return $this->hasOne('App\Models\Article', 'user_id', 'id');
    }

    public function user_data()
    {
        return $this->hasOne('App\Models\UserData', 'user_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'user_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review', 'user_id', 'id');
    }

    public function role()
    {
        return Sentinel::findById($this->id)->roles()->pluck('slug')->toArray();

    }

    /**
     * Получение списка желаний пользователя в виде массива id товаров или коллекции товаров
     * в зависимости от входящего аргумента
     *
     * @param $type = array or object
     * @return array|mixed|void
     */
    public function wishlist($type = 'object')
    {
        if (!is_null($this->user_data->wishlist)) {
            if ($type == 'array') {
                return json_decode($this->user_data->wishlist, true);
            } elseif ($type == 'object') {
                $product = new Product;
                return $product->getProducts(json_decode($this->user_data->wishlist, true));
            }
        } else {
            return [];
        }
    }

    public function checkIfUnregistered($phone, $email){
        return $this->where('email', $email)->orWhere('phone', $phone)->first();
    }

}
